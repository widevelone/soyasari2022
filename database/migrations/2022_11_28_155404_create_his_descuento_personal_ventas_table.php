<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHisDescuentoPersonalVentasTable extends Migration
{
    public function up()
    {
        Schema::create('his_descuento_personal_ventas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('descuento_personal_venta_id');
            $table->date('fecha');
            $table->string('detalle');
            $table->double('monto');
            $table->string('obs');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('his_descuento_personal_ventas');
    }
}
