<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHisPagoPasajeRefriCarrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('his_pago_pasaje_refri_carros', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pago_pasaje_refri_carros_id');
            $table->bigInteger('user_id');
            $table->integer('nro_dias');
            $table->double('monto');
            $table->string('obs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('his_pago_pasaje_refri_carros');
    }
}
