@extends('layouts.master')
{{--
@section('menu')
@extends('sidebar.dashboard')
@endsection --}}
@section('content')
    <div class="page-wrapper">
        <!-- Page Content -->
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title">Registrar Stok</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ '/tesoreria/descuentos' }}">Descuentos</a></li>
                            <li class="breadcrumb-item active"># {{ $descuento->id }}</li>
                        </ul>
                    </div>
                </div>
            </div>
            {{-- message --}}
            {!! Toastr::message() !!}
            <!-- /Page Header -->
            <div class="card mb-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-5 d-flex">

                            <div class="card-body">
                                <h3 class="card-title">Informacion General</h3>
                                <ul class="personal-info">
                                    {{-- <li>
                                        <div class="title">Tipo Negocio: </div>
                                        <div class="text-purple">
                                            {{ $almacen_personal->user->tipo_negocio }}
                                        </div>
                                    </li> --}}
                                    <li>
                                        <div class="title">Vendedor: </div>
                                        <div class="text-info">
                                            {{ $descuento->user->nombre . ' ' . $descuento->user->apellido_paterno . ' ' . $descuento->user->apellido_materno }}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">Total: </div>
                                        <div class="text-info">{{ $descuento->total }} <small>Bs.</small></div>
                                    </li>
                                    <li>
                                        <div class="title">Estado: </div>
                                        <div class="text-info">{{ $descuento->estado }}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @if ($descuento->estado == 'abierto')
                        <div class="row">
                            <div class="col">
                                <a href="#" class="btn btn-info" data-toggle="modal" data-target="#registrar_desc"><i
                                        class="fa fa-plus"></i>Adicionar</a>
                                <a href="#" class="btn btn-secondary" data-toggle="modal"
                                    data-target="#change_status"><i class="fa fa-plus"></i>Cerrar descuento</a>
                            </div>
                            {{-- <div class="col-sm-2">
                            </div> --}}
                        </div>
                    @endif
                </div>
            </div>


            <div class="tab-content">
                <div class="tab-pane fade show active" id="bank_statutory">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h3>KARDEX DE CONTROL FISICO</h3>
                            </center>
                            <table class="table table-striped custom-table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th hidden>id</th>
                                        <th>Fecha</th>
                                        <th>Detalle de descuento</th>
                                        <th>Monto</th>
                                        <th>Observaciones</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $x = 0;
                                    ?>
                                    @foreach ($historial_descuentos as $historial)
                                        <tr>
                                            <td hidden class="id">{{ $historial->id }}</td>
                                            <td class="fecha">{{ $historial->fecha }}</td>
                                            <td class="detalle">{{ $historial->detalle }}</td>
                                            <td class="monto text-danger">{{ $historial->monto }} <small> Bs.</small></td>
                                            <td class="obs">{{ $historial->obs }}</td>
                                            <td class="text-right">
                                                <?php
                                                $x++;
                                                ?>
                                                @can('borrar-registro-stok')
                                                    {{-- @if ($x == 1) --}}
                                                    @if ($descuento->estado == 'abierto')
                                                        <a class="btn btn-danger btn-sm userDelete" href="#"
                                                            data-toggle="modal" ata-id="'.$historial->id.'"
                                                            data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                    {{-- @endif --}}
                                                @endcan

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="registrar_desc" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrar Movimiento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('historialdesc/add/save') }}" method="POST" onsubmit="bloquear()"
                            id="formulario" onsubmit="bloquear()">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $descuento->id }}" id="descuento_personal_venta_id"
                                    name="descuento_personal_venta_id">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Detalles<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="detalle" id="detalle">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="col-form-label">Monto<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Bs</span>
                                        </div>
                                        <input required type="number" class="form-control" name="monto" id="monto"
                                            value="0" step="0.1" min="0.1">
                                    </div>
                                </div>
                                <div class="col-sm-4" id="fecha_reg">
                                    <label class="col-form-label">Fecha</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" name="fecha" id="fecha">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="col-form-label">Observaciones<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="obs" id="obs">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn"
                                    id="boton">Añadir</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- Delete User Modal -->
        <div class="modal custom-modal fade" id="delete_user" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Eliminar Registro</h3>
                            <p>¿Estás seguro de que quieres eliminar el registro?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('historialdesc/delete') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" class="e_id" value="">
                                <input type="hidden" name="descuento_id" value="{{ $descuento->id }}">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal custom-modal fade" id="change_status" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Cerrar Descuento</h3>
                            <p>¿Estás seguro de que desea cerrar el descuento?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('descuento/cerrar') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" value="{{ $descuento->id }}">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Cerrar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- delete js --}}
        <script>
            $(document).on('click', '.userDelete', function() {
                var _this = $(this).parents('tr');
                $('.e_id').val(_this.find('.id').text());
            });
        </script>
        {{-- delete js --}}
        <script>
            $(document).on('click', '.userDelete2', function() {
                var _this = $(this).parents('tr');
                $('.e_cuenta_id').val(_this.find('.cuenta_id').text());
                $('.e_hiscuenta_id').val(_this.find('.hiscuenta_id').text());
            });
        </script>


        <script>
            $(document).on('click', '.userDelete3', function() {
                var _this = $(this).parents('tr');

                $('.e_id').val(_this.find('.id').text());
            });
        </script>


        <script>
            function bloquear() {
                var btn = document.getElementById("boton");
                btn.disabled = true;
                $("#formulario :input").prop("readOnly", true);

            }
        </script>
        <script>
            function bloquear2() {
                var btn = document.getElementById("boton2");
                btn.disabled = true;
                $("#formulario2 :input").prop("readOnly", true);
            }
        </script>

        <script>
            function bloquear3() {
                var btn = document.getElementById("boton3");
                btn.disabled = true;
                $("#formulario3 :input").prop("readOnly", true);

            }
        </script>

        <script>
            function bloquear4() {
                var btn = document.getElementById("boton4");
                btn.disabled = true;
                $("#formulario4 :input").prop("readOnly", true);

            }
        </script>
    </div>
@endsection
