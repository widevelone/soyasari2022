@extends('layouts.master')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <!-- Page Content -->
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="page-title">Pagos de Refrigerio - Pasaje a Carros Distribuidores</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                            <li class="breadcrumb-item active">Pagos Refrigerio - Pasaje</li>
                        </ul>
                    </div>
                    <div class="col-auto float-right ml-auto">
                        @can('crear-stok')
                            <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i
                                    class="fa fa-plus"></i>Crear Stok</a>
                        @endcan
                    </div>
                </div>
            </div>
            <!-- /Page Header -->


            {{-- message --}}
            {!! Toastr::message() !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h3>PAGOS ABIERTOS</h3>
                            </center>
                            <table class="table table-striped custom-table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th hidden>id</th>
                                        <th hidden>Fecha</th>
                                        <th>Estado</th>
                                        <th>Total</th>
                                        <th>Fecha de Creación</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pago_ref_pas_carros_abierto as $dpv)
                                        <tr>
                                            <td hidden class="id">{{ $dpv->id }}</td>
                                            <td class="estado">{{ $dpv->estado }}</td>
                                            <td class="total">{{ $dpv->total }}</td>
                                            <td class="created_at">
                                                {{ \Carbon\Carbon::parse($dpv->created_at)->formatLocalized('%d de %B %Y') }}
                                            </td>
                                            <td class="text-right">
                                                 @can('editar-stok')
                                                    <a class="btn btn-success btn-sm"
                                                        href="{{ url('tesoreria/his_pago_ref_pas_carros/' . $dpv->id) }}"><i
                                                            class="fa fa-check"></i></a>
                                                @endcan
                                                @can('borrar-stok')
                                                    @if ($dpv->estado == 'abierto')
                                                        <a class="btn btn-danger btn-sm userDelete" href="#"
                                                            data-toggle="modal" ata-id="'.$dpv->id.'"
                                                            data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h3>PAGOS CERRADOS</h3>
                            </center>
                            <table class="table table-striped custom-table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th hidden>id</th>
                                        <th hidden>Fecha</th>
                                        <th>Estado</th>
                                        <th>Total</th>
                                        <th>Fecha de Creación</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($pago_ref_pas_carros_cerrado as $dpv)
                                        <tr>
                                            <td hidden class="id">{{ $dpv->id }}</td>

                                           
                                            <td class="estado">{{ $dpv->estado }}</td>
                                            <td class="total">{{ $dpv->total }}</td>
                                            <td class="created_at">
                                                {{ \Carbon\Carbon::parse($dpv->created_at)->formatLocalized('%d de %B %Y') }}
                                            </td>
                                            <td class="text-right">
                                                @can('editar-stok')
                                                    <a class="btn btn-success btn-sm"
                                                        href="{{ url('tesoreria/historialdescuentos/' . $dpv->id) }}"><i
                                                            class="fa fa-check"></i></a>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /Page Content -->

        <!-- Add User Modal -->
        <div id="add_user" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crear Descuento Personal Venta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('pagorpc/add/save') }}" method="POST" id="formulario"
                            onsubmit="bloquear()">
                            @csrf

                            <div class="col-sm-4" id="fecha_reg">
                                    <label class="col-form-label">Fecha</label>
                                    <div class="input-group">
                                        <input type="date" class="form-control" name="fecha" id="fecha">
                                    </div>
                            </div>
                            <br>
                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add User Modal -->


        <!-- Delete User Modal -->
        <div class="modal custom-modal fade" id="delete_user" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Eliminar Stok</h3>
                            <p>¿Estás seguro de que quieres eliminar el registro?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('descuento/delete') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" class="e_id" value="">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete User Modal -->


    </div>
    <!-- /Page Wrapper -->
@section('script')
    {{-- update js --}}

    {{-- delete js --}}
    <script>
        $(document).on('click', '.userDelete', function() {
            var _this = $(this).parents('tr');
            $('.e_id').val(_this.find('.id').text());

        });
    </script>

    <script>
        function bloquear() {
            var btn = document.getElementById("boton");
            $("#formulario :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>

    <script>
        function bloquear2() {
            var btn = document.getElementById("boton2");
            $("#formulario2 :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>
@endsection
@endsection
