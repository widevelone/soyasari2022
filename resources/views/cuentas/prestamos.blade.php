@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Prestamos de Turriles</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Prestamos</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>PRESTAMOS PENDIENTES</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Producto</th>
                                    <th>Turriles Prestados</th>
                                    <th>Turriles que Debe</th>
                                    <th>Estado</th>
                                    <th>Fecha Limite</th>
                                    <th>Fecha de prestamo</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuenta_turriles as $turril )
                                @if($turril->estado=='pendiente' && $turril->historial_cuaderno->metodo_vacio=='prestamo')
                                <tr>
                                    <td hidden class="id">{{ $turril->id }}</td>
                                    <td class="vendedor">{{$turril->historial_cuaderno->cuaderno->user->nombre.' '.$turril->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$turril->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$turril->historial_cuaderno->cliente->nombre.' '.$turril->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="re">{{$turril->historial_cuaderno->producto->nombre}}</td>
                                    <td class="monto">{{$turril->saldo_cantidad_inicial}}</td>
                                    <td class="saldo">{{$turril->saldo_cantidad}}</td>
                                    <td class="estado text-danger">{{$turril->estado}}</td>
                                    <td class="fecha_limite">{{$turril->fecha_limite}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($turril->created_at)->formatLocalized('%d de %B %Y')}}</td>

                                    <td class="text-right">
                                      
                                    {{-- @can('editar-cliente') --}}
                                        <a class="btn btn-info btn-sm edit_prestamo" data-toggle="modal"
                                                        data-id="'.$cuenta_turriles->id.'" data-target="#edit_prestamo"><i
                                                            class="fa fa-pencil"></i></a>
                                    {{-- @endcan --}}
                                    </td>

                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        <center>
                            <h3>PRESTAMOS FINALIZADOS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Producto</th>
                                    <th>Turriles Prestados</th>
                                    <th>Estado</th>
                                    <th>Fecha de entrega</th>
                                    <th>Fecha de prestamo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuenta_turriles as $turril )
                                @if($turril->estado=='entregado' && $turril->historial_cuaderno->metodo_vacio=='prestamo')
                                <tr>
                                    <td hidden class="id">{{ $turril->id }}</td>
                                    <td class="vendedor">{{$turril->historial_cuaderno->cuaderno->user->nombre.' '.$turril->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$turril->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$turril->historial_cuaderno->cliente->nombre.' '.$turril->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="re">{{$turril->historial_cuaderno->producto->nombre}}</td>
                                    <td class="monto">{{$turril->saldo_cantidad_inicial}}</td>

                                    <td class="estado text-success">{{$turril->estado}}</td>
                                    <td class="asd">{{$turril->updated_at}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($turril->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
        <div id="edit_prestamo" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Prestamos pendiente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('prestamos/fecha_limite/update') }}" enctype="multipart/form-data" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" id="e_id" hidden>
                        </div>
                        {{-- <div class="input-group">
                            <input type="text" class="form-control" name="fecha_limite" id="e_fecha_limite">
                        </div> --}}
                        <label class="col-form-label">Fecha Limite Actual</label>
                        <span class="text-danger">
                            <option value="{{ $turril->id }}">
                                <div class="fecha_limite_label"></div>
                            </option>
                        </span>
                        <label class="col-form-label">Nueva Fecha Limite</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="fecha_limite" id="e_fecha_limite">
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.edit_prestamo', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_fecha_limite').val(_this.find('.fecha_limite').text());

        const div = document.querySelector(".fecha_limite_label");
        div.textContent = _this.find('.fecha_limite').text();
    });
</script>

{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());
        $('.e_nombre').val(_this.find('.nombre').text());
        $('.e_foto_negocio').val(_this.find('.foto_negocio').text());
    });
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection