@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Detalle de Cuentas</h3>
                    <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="{{'/cuentas' }}">Cuentas</a></li>
                        <li class="breadcrumb-item active">{{$cuenta->id}}</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        {{-- message --}}
        {!! Toastr::message() !!}

        <div class="row">
                    
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Información General</h3>

                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Vendedor</div>
                                        <div class="text-info">{{$cuenta->historial_cuaderno->cuaderno->user->nombre.' '.$cuenta->historial_cuaderno->cuaderno->user->apellido_paterno}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Cliente</div>
                                        <div class="text-info">{{$cuenta->historial_cuaderno->cliente->nombre.' '.$cuenta->historial_cuaderno->cliente->apellido_paterno}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Monto Prestado</div>
                                        <div class="text-info">{{$cuenta->saldo_inicial}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Saldo</div>
                                        <div class="text-info">{{$cuenta->saldo}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Estado</div>
                                        <div class="text-info">{{$cuenta->estado}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Fecha de Prestamo</div>
                                        <div class="text-info">{{date('d-m-Y',strtotime($cuenta->created_at))}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Fecha Limite de Pago</div>
                                        <div class="text-info">{{date('d-m-Y',strtotime($cuenta->fecha_limite))}}</div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Monto Pagado</th>
                                    <th>Saldo</th>
                                    <th>Metodo de Pago</th>
                                    <th>Fecha de Pago</th>
                                    <th>Observaciones</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuenta->historial_cuenta as $detalle )
                                
                                <tr>
                                    <td hidden class="id">{{ $detalle->id }}</td>
                                    <td class="name">{{ $detalle->monto }}</td>
                                    <td class="usuarios_asignados">{{ $detalle->saldo }}</td>
                                    <td class="usuarios_asignadoss">{{ $detalle->metodo_pago }}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($detalle->created_at))}}</td>
                                    <td class="usuarios_asign233ados">{{ $detalle->obs }}</td>
                                
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

</div>
<!-- /Page Wrapper -->
@section('script')

@endsection

@endsection