@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Garantias</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Garantias</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        
                        <center>
                            <h3>GARANTIAS PENDIENTES</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Producto</th>
                                    <th>Turriles Prestados</th>
                                    <th>Monto de Garantia</th>
                                    <th>Estado</th>
                                    <th>Fecha de prestamo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuenta_turriles as $turril )
                                @if($turril->estado=='pendiente' && $turril->historial_cuaderno->metodo_vacio=='garantia')
                                <tr>
                                    <td hidden class="id">{{ $turril->id }}</td>
                                    <td class="vendedor">{{$turril->historial_cuaderno->cuaderno->user->nombre.' '.$turril->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$turril->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$turril->historial_cuaderno->cliente->nombre.' '.$turril->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="re">{{$turril->historial_cuaderno->producto->nombre}}</td>
                                    <td class="monto">{{$turril->saldo_cantidad_inicial}}</td>
                                    <td class="saldo">{{$turril->saldo_garantia_inicial}}</td>
                                    <td class="estado text-danger">{{$turril->estado}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($turril->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        <center>
                            <h3>GARANTIAS FINALIZADAS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Producto</th>
                                    <th>Turriles Prestados</th>
                                    <th>Monto de Garantia</th>
                                    <th>Estado</th>
                                    <th>Fecha de pago</th>
                                    <th>Fecha de prestamo</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuenta_turriles as $turril )
                                @if($turril->estado=='entregado' && $turril->historial_cuaderno->metodo_vacio=='garantia')
                                <tr>
                                    <td hidden class="id">{{ $turril->id }}</td>
                                    <td class="vendedor">{{$turril->historial_cuaderno->cuaderno->user->nombre.' '.$turril->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$turril->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$turril->historial_cuaderno->cliente->nombre.' '.$turril->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="re">{{$turril->historial_cuaderno->producto->nombre}}</td>
                                    <td class="monto">{{$turril->saldo_cantidad_inicial}}</td>
                                    <td class="saldo">{{$turril->saldo_garantia_inicial}}</td>
                                    <td class="estado text-success">{{$turril->estado}}</td>
                                    <td class="asd">{{$turril->updated_at}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($turril->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- /Page Wrapper -->
@section('script')

{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());
        $('.e_nombre').val(_this.find('.nombre').text());
        $('.e_foto_negocio').val(_this.find('.foto_negocio').text());
    });
</script>
@endsection

@endsection