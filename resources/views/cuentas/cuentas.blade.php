@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Cuentas</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Cuentas</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>DEUDAS PENDIENTES</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">

                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Monto</th>
                                    <th>Saldo</th>
                                    <th>Estado</th>
                                    <th>Fecha Limite</th>
                                    <th>Fecha de Deuda</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuentas_pen as $cuenta_pen )
                                
                                <tr>
                                    <td hidden class="id">{{ $cuenta_pen->id }}</td>
                                    <td class="vendedor">{{$cuenta_pen->historial_cuaderno->cuaderno->user->nombre.' '.$cuenta_pen->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$cuenta_pen->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$cuenta_pen->historial_cuaderno->cliente->nombre.' '.$cuenta_pen->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="monto">{{$cuenta_pen->saldo_inicial}}</td>
                                    <td class="saldo">{{$cuenta_pen->saldo}}</td>
                                    <td class="estado text-danger">{{$cuenta_pen->estado}}</td>
                                    <td class="fecha_limite">{{\Carbon\Carbon::parse($cuenta_pen->fecha_limite)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($cuenta_pen->created_at)->formatLocalized('%d de %B %Y')}}</td>

                                    
                                    <td class="text-right">
                                        <a class="btn btn-success btn-sm" href="{{ url('cuentas/detalle/'.$cuenta_pen->id) }}"><i class="fa fa-eye"></i></a>
                                    {{-- @can('editar-cliente') --}}
                                        <a class="btn btn-info btn-sm edit_cuenta" data-toggle="modal"
                                                        data-id="'.$cuenta_pen->id.'" data-target="#edit_cuenta"><i
                                                            class="fa fa-pencil"></i></a>
                                    {{-- @endcan --}}
                                    </td>
                                    
                                </tr>
                                
                                @endforeach

                            


                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>DEUDAS PAGADAS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>ID</th>
                                    <th>Vendedor</th>
                                    <th>Cuaderno</th>
                                    <th>Cliente</th>
                                    <th>Monto</th>
                                    <th>Saldo</th>
                                    <th>Estado</th>
                                    <th>Fecha de Cobro</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuentas_pag as $cuenta_pag )
                                <tr>
                                    <td hidden class="id">{{ $cuenta_pag->id }}</td>
                                    <td class="vendedor">{{$cuenta_pag->historial_cuaderno->cuaderno->user->nombre.' '.$cuenta_pag->historial_cuaderno->cuaderno->user->apellido_paterno}}</td>
                                    <td class="historial_cuaderno_id">{{$cuenta_pag->historial_cuaderno->cuaderno->codigo}}</td>
                                    <td class="cliente">{{$cuenta_pag->historial_cuaderno->cliente->nombre.' '.$cuenta_pag->historial_cuaderno->cliente->apellido_paterno}}</td>
                                    <td class="monto">{{$cuenta_pag->saldo_inicial}}</td>
                                    <td class="saldo">{{$cuenta_pag->saldo}}</td>

                                    <td class="estado text-success">{{$cuenta_pag->estado}}</td>
                                    <td class="created_at">{{\Carbon\Carbon::parse($cuenta_pag->update_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        <a class="btn btn-success btn-sm" href="{{ url('cuentas/detalle/'.$cuenta_pag->id) }}"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div id="edit_cuenta" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Cuenta pendiente</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('cuentas/fecha_limite/update') }}" enctype="multipart/form-data" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" id="e_id" hidden>
                        </div>
                        {{-- <div class="input-group">
                            <input type="text" class="form-control" name="fecha_limite" id="e_fecha_limite" hidden>
                        </div> --}}
                        <label class="col-form-label">Fecha Limite Actual</label>
                        <span class="text-danger">
                            <option value="{{ $cuenta_pen->id }}">
                                <div class="fecha_limite_label"></div>
                            </option>
                        </span>
                        <label class="col-form-label">Nueva Fecha Limite</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="fecha_limite" id="e_fecha_limite">
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.edit_cuenta', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_fecha_limite').val(_this.find('.fecha_limite').text());

        const div = document.querySelector(".fecha_limite_label");
        div.textContent = _this.find('.fecha_limite').text();
    });
</script>

{{-- delete js --}}
<script>
    $(document).on('click', '.edit_cuenta', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());
        
        $('.e_foto_negocio').val(_this.find('.foto_negocio').text());
    });
</script>
<script>
    $(document).on('click', '.edit_cuaderno', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_created_at').val(_this.find('.created_at').text());
        const div = document.querySelector(".created_at_label");
        div.textContent = _this.find('.created_at').text();
    });
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection