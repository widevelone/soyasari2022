@extends('layouts.master')
{{--
@section('menu')
@extends('sidebar.dashboard')
@endsection --}}
@section('content')


<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Registrar Deudas anteriores</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Deudas anteriores</li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- message --}}
        {!! Toastr::message() !!}
        <!-- /Page Header -->
        <div class="card mb-0">
            <div class="card-body">

                <div class="row">

                    <div class="col-sm">
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#registrar_deuda">Deuda</a>
                    </div>
                    <div class="col-sm">
                        <a href="#" class="btn btn-info" data-toggle="modal" data-target="#cobranza">Prestamo</a>
                    </div>
                    <div class="col-sm">
                        <a href="#" class="btn btn-warning" data-toggle="modal" data-target="#registrar_gasto">Garantia</a>
                    </div>

                </div>

            </div>
        </div>



    </div>



    <div id="registrar_deuda" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Deuda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('anterior/add/save') }}" method="POST" onsubmit="bloquear()" id="formulario" onsubmit="bloquear()">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="col-form-label">Cliente <span class="text-danger">*</span></label>
                                <select required class="js-example-basic-single" style="width: 100%;" tabindex="-1" aria-hidden="true" require name="cliente_id" id="cliente_id">
                                    <option disabled selected value=""> --Seleccione--</option>
                                    @foreach($clientes as $cliente)
                                    <option value="{{ $cliente->id }}">{{ $cliente->nombre.' '.$cliente->apellido_paterno}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-form-label">Producto<span class="text-danger">*</span></label>
                                    <select required class="select" name="producto_id" id="producto_id">
                                        <option disabled selected value=""> --Seleccione--</option>
                                        @foreach($productos as $producto)
                                        <option value="{{ $producto->id }}">{{$producto->nombre.', '.$producto->capacidad.', Bs. '.$producto->precio}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-form-label">Precio Unitario</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Bs</span>
                                        </div>
                                        <input required type="number" class="form-control" name="precio_unitario" id="precio_unitario" value="0" oninput="myFunction()">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label class="col-form-label">Cantidad<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <input required type="number" class="form-control" value="0" name="cantidad" id="cantidad" oninput="myFunction()">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-form-label">Total Venta<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input required type="number" class="form-control" value="0" name="total_venta" id="total_venta" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-form-label">Total Cobrado</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input required type="number" class="form-control" value="0" name="total_cobrado" id="total_cobrado" oninput="myFunction()">
                                </div>
                            </div>
                        </div>

                        <br>
                        <div class="row">

                            <div class="col-sm-4">
                                <label class="col-form-label">Saldo<span class="text-danger">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input required type="number" class="form-control" value="0" name="total_saldo" id="total_saldo" readonly>
                                </div>
                            </div>
                            <div class="col-sm-4" id="fecha_cuenta">
                                <label class="col-form-label">Fecha Limite Deuda<span class="text-danger">*</span></label>
                                <div class="input-group">

                                    <input required type="date" class="form-control" name="fecha_limite" id="fecha_limite">
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn" id="boton">Añadir</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div id="cobranza" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cobrar Deuda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('hiscuenta/add/save') }}" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <div class="row">


                            <div class="col-sm-6">
                                <label class="col-form-label">Cliente <span class="text-danger">*</span></label>
                                <select required class="select" name="cuenta" id="cuenta">
                                    <option disabled selected value=""> --Seleccione--</option>

                                </select>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">Monto Cobrado</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Bs</span>
                                        </div>
                                        <input required type="number" class="form-control" name="monto" id="monto" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-form-label">Metodo de Pago<span class="text-danger">*</span></label>
                                <select required class="select" name="metodo_pago_c" id="metodo_pago_c">
                                    <option value="Efectivo">Efectivo</option>
                                    <option value="Transferencia">Transferencia</option>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm">
                                <label class="col-form-label">Observaciones<span class="text-danger">*</span></label>
                                <div class="input-group">

                                    <input type="text" class="form-control" name="obs_c" id="obs_c">
                                </div>
                            </div>

                        </div>

                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Añadir</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="registrar_gasto" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Gasto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('gasto/add/save') }}" method="POST" id="formulario3" onsubmit="bloquear3()">
                        @csrf
                        <div class="row">


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">Descripción</label>
                                    <input required type="text" class="form-control" name="descripcion" id="descripcion" placeholder="ej: Flete">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">Monto</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Bs</span>
                                        </div>
                                        <input required type="number" class="form-control" name="monto_gasto" id="monto_gasto" value="0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton3" class="btn btn-primary submit-btn">Añadir</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div id="registrar_turril" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Turril</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('turril/add/save') }}" method="POST" id="formulario4" onsubmit="bloquear4()">
                        @csrf
                        <div class="row">

                            <div class="col-sm-6">
                                <label class="col-form-label">Cliente <span class="text-danger">*</span></label>
                                <select required class="select" name="cuenta_turril_id" id="cuenta_turril_id">
                                    <option disabled selected value=""> --Seleccione--</option>

                                </select>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-form-label">Cantidad de Turriles Recogidos<span class="text-danger">*</span></label>
                                <input required type="number" class="form-control" name="cantidad_turriles" id="cantidad_turriles" value="0">
                            </div>
                        </div>
                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton4" class="btn btn-primary submit-btn">Añadir</button>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <div id="registrar_efectivo" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Registrar Efectivo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('efectivo/add/save') }}" method="POST" id="formulario5" onsubmit="bloquear5()">
                        @csrf
                        <div class="row">

                            <div class="col-sm-6">
                                <label class="col-form-label">Monedas</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input required type="number" class="form-control" name="moneda" id="moneda">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-form-label">Billetes</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input required type="number" class="form-control" name="billete" id="billete">
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-sm-6">
                                <label class="col-form-label">Dolares</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                    </div>
                                    <input required type="number" class="form-control" name="dolar" id="dolar">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class="col-form-label">Extensible<span class="text-danger">*</span></label>
                                <input disabled type="number" class="form-control" name="otros" id="otros" value="0">
                            </div>
                        </div>

                        <br>
                        <div class="submit-section">
                            <button type="submit" id="boton5" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar venta</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('his/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="id" class="e_id" value="">

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user2" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Cobro</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('hiscuenta/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="hiscuenta_id" class="e_hiscuenta_id" value="">
                            <input type="hidden" name="cuenta_id" class="e_cuenta_id" value="">
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user3" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Gasto</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('gasto/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="id" class="e_id" value="">
                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal custom-modal fade" id="cerrar" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Cerrar el Cuaderno</h3>
                        <p>¿Estás seguro de que quieres cerrar el cuaderno?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('cuaderno/cerrar') }}" method="POST">
                            @csrf


                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Cerrar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="detalle_venta" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Detalle de la Venta</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-4">
                            <label class="col-form-label">Cliente</label>
                            <input type="text" class="form-control" name="e_cliente" id="e_cliente" readonly>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="col-form-label">Producto</label>
                                <input type="text" class="form-control" name="e_producto" id="e_producto" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="col-form-label">Precio Unitario</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Bs</span>
                                    </div>
                                    <input type="number" class="form-control" name="e_precio_unitario" id="e_precio_unitario" readonly>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="col-form-label">Cantidad</label>
                            <div class="input-group">
                                <input type="number" class="form-control" name="e_cantidad" id="e_cantidad" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-form-label">Cantidad de Vacios Recogidos</label>
                            <div class="input-group">
                                <input type="number" class="form-control" value="0" name="e_cantidad_vacio" id="e_cantidad_vacio" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="metodo_vacio" class="col-form-label">Metodo de intercambio</label>
                            <input type="text" class="form-control" name="e_metodo_vacio" id="e_metodo_vacio" readonly>
                        </div>
                    </div>
                    <br>

                    <div class="row">
                        <div class="col-sm-4">
                            <label class="col-form-label">Estado del Contenedor</label>
                            <input type="text" class="form-control" name="e_estado_turril" id="e_estado_turril" readonly>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-form-label">Metodo de Pago</label>
                            <input type="text" class="form-control" name="e_metodo_pago" id="e_metodo_pago" readonly>
                        </div>
                        <div class="col-sm-4" id="fecha_prestamo" style="display: none">
                            <label class="col-form-label">Fecha Limite Devolución</label>
                            <div class="input-group">
                                <input type="date" class="form-control" name="e_fecha_limite_turril" id="e_fecha_limite_turril" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4" id="monto_garantia_div" style="display: none">
                            <label class="col-form-label">Monto de Garantia</label>
                            <div class="input-group">
                                <input type="number" class="form-control" name="e_monto_garantia" id="e_monto_garantia" readonly>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="col-form-label">Total Venta</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Bs</span>
                                </div>
                                <input type="number" class="form-control" name="e_total_venta" id="e_total_venta" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-form-label">Total Cobrado</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Bs</span>
                                </div>
                                <input type="number" class="form-control" name="e_total_cobrado" id="e_total_cobrado" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-form-label">Saldo</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Bs</span>
                                </div>
                                <input type="number" class="form-control" name="e_total_deuda" id="e_total_deuda" readonly>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-8">
                            <label class="col-form-label">Observaciones</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="e_obs" id="e_obs" readonly>
                            </div>
                        </div>
                        <div class="col-sm-4" id="e_fecha_cuenta" style="display: none">
                            <label class="col-form-label">Fecha Limite Deuda</label>
                            <div class="input-group">

                                <input type="date" class="form-control" name="e_fecha_limite_cuenta" id="e_fecha_limite_cuenta" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- /Delete User Modal -->



    <script>
        function myFunction() {
            var x = document.getElementById("cantidad").value;
            var y = document.getElementById("precio_unitario").value;
            document.getElementById("total_venta").value = x * y;
            var venta = document.getElementById("total_venta").value;
            var cobrado = document.getElementById("total_cobrado").value;
            var saldo = venta - cobrado;
            document.getElementById("total_saldo").value = saldo;
            var cantidad_vacio = document.getElementById("cantidad_vacio").value;

            if (x - cantidad_vacio != 0) {
                document.getElementById("Normal").disabled = true;
                document.getElementById("Normal").value = "";

                document.getElementById("Garantia").disabled = false;
                document.getElementById("Garantia").value = "garantia";

                document.getElementById("Prestamo").disabled = false;
                document.getElementById("Prestamo").value = "prestamo";
            } else {
                document.getElementById("Normal").disabled = false;
                document.getElementById("Normal").value = "normal";

                document.getElementById("Garantia").disabled = true;
                document.getElementById("Garantia").value = "";

                document.getElementById("Prestamo").disabled = true;
                document.getElementById("Prestamo").value = "";
            }

            var select_ = document.getElementById("metodo_vacio").value;
            if (select_ == "normal") {
                document.getElementById("monto_garantia_div").style.display = "none";
                document.querySelector('#monto_garantia').required = false;

                document.getElementById("fecha_prestamo").style.display = "none";
                document.querySelector('#fecha_limite_turril').required = false;
            }

            if (select_ == "garantia") {
                document.getElementById("monto_garantia_div").style.display = "block";
                document.querySelector('#monto_garantia').required = true;

                document.getElementById("fecha_prestamo").style.display = "none";
                document.querySelector('#fecha_limite_turril').required = false;
            }

            if (select_ == "prestamo") {
                document.getElementById("monto_garantia_div").style.display = "none";
                document.querySelector('#monto_garantia').required = false;

                document.getElementById("fecha_prestamo").style.display = "block";
                document.querySelector('#fecha_limite_turril').required = true;
            }


            if (saldo == 0) {
                document.getElementById("fecha_cuenta").style.display = "none";
                document.querySelector('#fecha_limite_cuenta').required = false;
            } else {
                document.getElementById("fecha_cuenta").style.display = "block";
                document.querySelector('#fecha_limite_cuenta').required = true;
            }
        }
    </script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });
    </script>





    {{-- delete js --}}
    <script>
        $(document).on('click', '.userDelete', function() {
            var _this = $(this).parents('tr');
            $('.e_id').val(_this.find('.id').text());
        });
    </script>
    {{-- delete js --}}
    <script>
        $(document).on('click', '.userDelete2', function() {
            var _this = $(this).parents('tr');
            $('.e_cuenta_id').val(_this.find('.cuenta_id').text());
            $('.e_hiscuenta_id').val(_this.find('.hiscuenta_id').text());
        });
    </script>


    <script>
        $(document).on('click', '.userDelete3', function() {
            var _this = $(this).parents('tr');

            $('.e_id').val(_this.find('.id').text());
        });
    </script>


    <script>
        function bloquear() {
            var btn = document.getElementById("boton");
            btn.disabled = true;
            $("#formulario :input").prop("readOnly", true);

        }
    </script>
    <script>
        function bloquear2() {
            var btn = document.getElementById("boton2");
            btn.disabled = true;
            $("#formulario2 :input").prop("readOnly", true);
        }
    </script>

    <script>
        function bloquear3() {
            var btn = document.getElementById("boton3");
            btn.disabled = true;
            $("#formulario3 :input").prop("readOnly", true);

        }
    </script>

    <script>
        function bloquear4() {
            var btn = document.getElementById("boton4");
            btn.disabled = true;
            $("#formulario4 :input").prop("readOnly", true);

        }
    </script>

    <script>
        function bloquear5() {
            var btn = document.getElementById("boton5");
            btn.disabled = true;
            $("#formulario5 :input").prop("readOnly", true);

        }
    </script>


    <script>
        $(document).on('click', '.detalle', function() {
            var _this = $(this).parents('tr');
            $('#e_cliente').val(_this.find('.cliente').text());
            $('#e_producto').val(_this.find('.producto').text());
            $('#e_cantidad').val(_this.find('.cantidad').text());
            $('#e_precio_unitario').val(_this.find('.precio_unitario').text());
            $('#e_metodo_pago').val(_this.find('.metodo_pago').text());
            $('#e_total_venta').val(_this.find('.total_venta').text());
            $('#e_total_cobrado').val(_this.find('.total_cobrado').text());
            $('#e_total_deuda').val(_this.find('.total_deuda').text());
            $('#e_estado_turril').val(_this.find('.estado_turril').text());
            $('#e_cantidad_vacio').val(_this.find('.cantidad_vacio').text());
            $('#e_metodo_vacio').val(_this.find('.metodo_vacio').text());
            $('#e_obs').val(_this.find('.obs').text());

            var saldo = document.getElementById("e_total_deuda").value;
            var metodo = document.getElementById("e_metodo_vacio").value;



        });
    </script>


</div>

@endsection