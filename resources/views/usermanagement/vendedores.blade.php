@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Vendedores</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Vendedores</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">

                            <thead>
                                <tr>
                                    <th>Codigo</th>
                                    <th>Nombre</th>
                                    <th hidden>Apellido Paterno</th>
                                    <th hidden>Apellido Materno</th>
                                    <th hidden>Nombre</th>
                                    <th hidden>Nombre</th>
                                    <th>Clientes</th>
                                    <th hidden>Carnet</th>
                                    <th>Celular</th>
                                    <th hidden>Direccion</th>
                                    <th>Departamento</th>
                                    <th hidden>Departamento_id</th>
                                    <th>Estado</th>
                                    <th hidden>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user )
                                <tr>

                                    <td>
                                        <span hidden class="image">{{ $user->avatar}}</span>

                                        <h2 class="table-avatar">
                                            <a class="avatar"><img src="{{ URL::to('/assets/images/'. $user->avatar) }}" alt="{{ $user->avatar }}"></a>
                                            <a class="email">{{ $user->email }}</span></a>
                                        </h2>

                                    </td>


                                    <td>{{$user->nombre}} {{$user->apellido_paterno}} {{$user->apellido_materno}}</td>
                                    <td hidden class="ids">{{ $user->id }}</td>
                                    <td hidden class="name">{{ $user->nombre }}</td>
                                    <td hidden class="apellido_paterno">{{ $user->apellido_paterno }}</td>
                                    <td hidden class="apellido_materno">{{ $user->apellido_materno }}</td>
                                    <td>
                                        <?php
                                        $cont = 0;
                                        ?>
                                        @foreach($user->cliente as $cliente)
                                        <?php
                                        $cont = $cont + 1;
                                        ?>
                                        @endforeach
                                        <span class="badge bg-inverse-success">{{$cont}}</span>
                                    </td>
                                    <td hidden class="carnet">{{ $user->carnet }}</td>
                                    <td class="phone_number">{{ $user->telefono }}</td>
                                    <td hidden class="direccion">{{ $user->direccion }}</td>


                                    <td class="department">{{ $user->departamento->nombre }}</td>
                                    <td hidden class="department_id">{{ $user->departamento->id  }}</td>


                                    <td>
                                        <div class="dropdown action-label">
                                            @if ($user->estado=='Activo')
                                            <a style="color:green">
                                                <i class="fa fa-dot-circle-o"></i>
                                                <span class="statuss">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='Inactivo')
                                            <a style="color:blue">
                                                <i class="fa fa-dot-circle-o text-info"></i>
                                                <span class="statuss text-info">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='Deshabilitado')
                                            <a style="color:red">
                                                <i class="fa fa-dot-circle-o "></i>
                                                <span class="statuss">{{ $user->estado }}</span>
                                            </a>
                                            @elseif ($user->estado=='')
                                            <a>
                                                <i class="fa fa-dot-circle-o text-dark"></i>
                                                <span class="statuss text-dark">N/A</span>
                                            </a>
                                            @endif


                                        </div>
                                    </td>
                                    <td hidden class="created_at">{{\Carbon\Carbon::parse($user->created_at)->formatLocalized('%d de %B %Y')}}</td>
                                    <td class="text-right">
                                        @can('ver-perfil-vendedor')
                                        <a class="btn btn-success btn-sm" href="{{ url('vendedores/perfil/'.$user->id) }}"><i class="fa fa-eye"></i></a>
                                        @endcan

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

</div>
<!-- /Page Wrapper -->

@endsection