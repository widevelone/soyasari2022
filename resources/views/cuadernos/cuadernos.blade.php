@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Cuadernos Diarios</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Cuadernos</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-cuaderno')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Cuaderno</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->


        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>CUADERNOS ABIERTOS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Codigo</th>
                                    <th>Vendedor</th>
                                    <th>Venta</th>
                                    <th hidden>Cobranza</th>
                                    <th hidden>Gasto</th>
                                    <th>Garantia</th>
                                    <th>Deuda</th>
                                    <th>Monto Final</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>


                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuadernos as $cuaderno )
                                <tr>
                                    <td hidden class="id">{{ $cuaderno->id }}</td>

                                    <td> <span>
                                            @can('editar-cuaderno')
                                            <a class="btn btn-warning btn-sm" href="{{ url('cuadernos/registrar/'.$cuaderno->id) }}"><i class="fa fa-usd"></i></a>
                                            @endcan
                                        </span>
                                        <span class="text-danger">
                                            {{$cuaderno->codigo}}
                                        </span>
                                    </td>


                                    <?php
                                    $var1 = $cuaderno->user->nombre;
                                    $var2 = $cuaderno->user->apellido_paterno;
                                    $mostrar = $var1 . ' ' . $var2;
                                    ?>
                                    <td class="vendedor">{{$mostrar}}</td>
                                    <td class="total_venta">{{ $cuaderno->total_venta }}</td>
                                    <td hidden class="total_cobranza">{{ $cuaderno->total_cobranza }}</td>
                                    <td hidden class="total_gasto">{{ $cuaderno->total_gasto }}</td>
                                    <td class="total_garantia text-warning">{{ $cuaderno->total_garantia}}</td>
                                    <td class="total_deuda text-danger">{{ $cuaderno->total_deuda}}</td>
                                    <td class="total_cuaderno text-success">{{ $cuaderno->total_cuaderno}}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($cuaderno->created_at))}}</td>


                                    <!--Fecha-->
                                    <td class="text-right">
                                        @can('editar-cuaderno')
                                        <a class="btn btn-info btn-sm edit_cuaderno" data-toggle="modal" data-id="'.$cuaderno->id.'" data-target="#edit_cuaderno"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                    </td>



                                    <!--endFecha-->



                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                        <center>
                            <h3>CUADERNOS CERRADOS</h3>
                        </center>
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Codigo</th>
                                    <th>Vendedor</th>
                                    <th>Venta</th>
                                    <th hidden>Cobranza</th>
                                    <th hidden>Gasto</th>
                                    <th>Garantia</th>
                                    <th>Deuda</th>
                                    <th>Monto Final</th>
                                    <th>Fecha de Alta</th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cuadernosc as $cuaderno )
                                <tr>
                                    <td hidden class="id">{{ $cuaderno->id }}</td>

                                    <td> <span>
                                            @can('editar-cuaderno')
                                            <a class="btn btn-warning btn-sm" href="{{ url('cuadernos/registrar/'.$cuaderno->id) }}"><i class="fa fa-usd"></i></a>
                                            @endcan
                                        </span>
                                        <span class="text-success">
                                            {{$cuaderno->codigo}}
                                        </span>
                                    </td>


                                    <?php
                                    $var1 = $cuaderno->user->nombre;
                                    $var2 = $cuaderno->user->apellido_paterno;
                                    $mostrar = $var1 . ' ' . $var2;
                                    ?>
                                    <td class="vendedor">{{$mostrar}}</td>
                                    <td class="total_venta">{{ $cuaderno->total_venta }}</td>
                                    <td hidden class="total_cobranza">{{ $cuaderno->total_cobranza }}</td>
                                    <td hidden class="total_gasto">{{ $cuaderno->total_gasto }}</td>
                                    <td class="total_garantia text-warning">{{ $cuaderno->total_garantia}}</td>
                                    <td class="total_deuda text-danger">{{ $cuaderno->total_deuda}}</td>
                                    <td class="total_cuaderno text-success">{{ $cuaderno->total_cuaderno}}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($cuaderno->created_at))}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>



                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Cuaderno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('cuaderno/add/save') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf
                        <div class="form-group">
                            <label for="user_id">Vendedor</label>
                            <select required class="select" name="user_id" id="user_id">
                                <option disabled selected value=""> --Seleccione--</option>
                                @foreach($usuarios as $usuario)
                                @if($usuario->departamento->nombre=="Ventas" && $usuario->estado=="Activo")
                                <?php
                                $existe = false;
                                ?>
                                @foreach ($cuadernos as $cuaderno)
                                <?php

                                if ($cuaderno->estado == 'abierto' && $cuaderno->user_id == $usuario->id) {
                                    $existe = true;
                                }
                                ?>
                                @endforeach
                                @if(!$existe)
                                <option value="{{ $usuario->id }}">{{ $usuario->nombre.' '.$usuario->apellido_paterno }}</option>
                                @endif

                                @endif

                                @endforeach
                            </select>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->



    <!--Fecha-->

    <div id="edit_cuaderno" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Cuaderno</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <br>
                <div class="modal-body">
                    <form action="{{ route('cuaderno/created_at/update') }}" enctype="multipart/form-data" method="POST" id="formulario2" onsubmit="bloquear2()">
                        @csrf
                        <div class="input-group">
                            <input type="text" class="form-control" name="id" id="e_id" hidden>
                        </div>
                        <label class="col-form-label">Fecha Actual</label>
                        <span class="text-danger">
                            <option value="{{ $cuaderno->id }}">
                                <div class="created_at_label"></div>
                            </option>
                        </span>
                        <label class="col-form-label">Nueva Fecha</label>
                        <div class="input-group">
                            <input type="date" class="form-control" name="created_at" id="e_created_at">
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton2" class="btn btn-primary submit-btn">Actualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--endFecha-->




</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.edit_cuaderno', function() {
        var _this = $(this).parents('tr');
        $('#e_id').val(_this.find('.id').text());
        $('#e_created_at').val(_this.find('.created_at').text());
        const div = document.querySelector(".created_at_label");
        div.textContent = _this.find('.created_at').text();
    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.ids').text());
        $('.e_avatar').val(_this.find('.image').text());
    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>

<script>
    function bloquear2() {
        var btn = document.getElementById("boton2");
        $("#formulario2 :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>
@endsection

@endsection