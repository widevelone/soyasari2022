@extends('layouts.master')
@section('content')
    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <!-- Page Content -->
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="page-title">Administrador de Almacenes personales</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                            <li class="breadcrumb-item active">Almacenes</li>
                        </ul>
                    </div>
                    <div class="col-auto float-right ml-auto">
                        @can('crear-stok')
                            <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i
                                    class="fa fa-plus"></i>Crear Stok</a>
                        @endcan
                    </div>
                </div>
            </div>
            <!-- /Page Header -->


            {{-- message --}}
            {!! Toastr::message() !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped custom-table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th hidden>id</th>
                                        <th>Tipo Negocio</th>
                                        <th>Usuario</th>
                                        <th>Producto</th>
                                        <th>Estado</th>
                                        <th hidden>Cantidad Anterior</th>
                                        <th>Cantidad Actual</th>
                                        <th>Ultima Actualización</th>
                                        <th>Fecha de Alta</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($almacen_personales as $almacen)
                                        <tr>
                                            <td hidden class="id">{{ $almacen->id }}</td>

                                            <td>
                                                <span hidden class="image">{{ $almacen->user->avatar }}</span>
                                                <h2 class="table-avatar">
                                                    <a class="avatar"> <img
                                                            src="{{ URL::to('/assets/images/' . $almacen->user->avatar) }}"
                                                            alt="{{ $almacen->user->avatar }}"></a>
                                                    {{ $almacen->user->tipo_negocio }} <br>                                                
                                                    </span></a>
                                                </h2>
                                            </td>



                                            <td class="usuario">
                                                {{ $almacen->user->nombre. ' ' . $almacen->user->apellido_paterno . ' ' . $almacen->user->apellido_materno }}
                                            </td>

                                            <td class="producto">
                                                {{ $almacen->producto->nombre }}
                                            </td>
                                            <td class="estado">{{ $almacen->estado }}</td>
                                            <td hidden class="cantidad_anterior">{{ $almacen->cantidad_anterior }}</td>
                                            <td class="cantidad_actual text-info">{{ $almacen->cantidad_actual }}</td>
                                            <td class="update_at">
                                                {{ \Carbon\Carbon::parse($almacen->updated_at)->formatLocalized('%d de %B %Y') }}
                                            </td>
                                            <td class="created_at">
                                                {{ \Carbon\Carbon::parse($almacen->created_at)->formatLocalized('%d de %B %Y') }}
                                            </td>
                                            <td class="text-right">
                                                @can('editar-stok')
                                                    <a class="btn btn-success btn-sm"
                                                        href="{{ url('almacenes_personales/registrar/' . $almacen->id) }}"><i
                                                            class="fa fa-check"></i></a>
                                                @endcan
                                                @can('borrar-stok')
                                                    @if ($almacen->historial_almacen_personal == '[]')
                                                        <a class="btn btn-danger btn-sm userDelete" href="#"
                                                            data-toggle="modal" ata-id="'.$almacen_personales->id.'"
                                                            data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Content -->

        <!-- Add User Modal -->
        <div id="add_user" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Crear Stok</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('almacen_personal/add/save') }}" method="POST" id="formulario"
                            onsubmit="bloquear()">
                            @csrf

                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="user_id">Cliente</label>
                                    <select required class="select" name="user_id" id="user_id">
                                        <option disabled selected value=""> --Seleccione--</option>
                                        @foreach ($clientes as $cliente)
                                     
                                            <option value="{{ $cliente->user_id }}">
                                                {{ $cliente->codigo . ' ' . $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno. ', ' . $cliente->tipo_negocio }}
                                            </option>
                                            
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <br>

                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="user_id">Producto</label>
                                    <select required class="select" name="producto_id" id="producto_id">
                                        <option disabled selected value=""> --Seleccione--</option>
                                        @foreach ($productos as $producto)
                                            <option value="{{ $producto->id }}">
                                                {{ $producto->nombre . ' ' . $producto->descripcion . ' ' . $producto->capacidad }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>                      

                            <br>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Estado<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="estado" id="estado">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">Cantidad Actual<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="number" class="form-control" value="0"
                                            name="cantidad_actual" id="cantidad_actual">
                                    </div>
                                </div>
                            </div>
                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add User Modal -->


        <!-- Delete User Modal -->
        <div class="modal custom-modal fade" id="delete_user" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Eliminar Stok</h3>
                            <p>¿Estás seguro de que quieres eliminar el registro?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('almacen_personal/delete') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" class="e_id" value="">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete User Modal -->


    </div>
    <!-- /Page Wrapper -->
@section('script')
    {{-- update js --}}

    {{-- delete js --}}
    <script>
        $(document).on('click', '.userDelete', function() {
            var _this = $(this).parents('tr');
            $('.e_id').val(_this.find('.id').text());

        });
    </script>

    <script>
        function bloquear() {
            var btn = document.getElementById("boton");
            $("#formulario :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>

    <script>
        function bloquear2() {
            var btn = document.getElementById("boton2");
            $("#formulario2 :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>
@endsection
@endsection
