@extends('layouts.master')
{{--
@section('menu')
@extends('sidebar.dashboard')
@endsection --}}
@section('content')
    <div class="page-wrapper">
        <!-- Page Content -->
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="page-title">Registrar Stok</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="{{ '/almacenes_personales' }}">Almacenes</a></li>
                            <li class="breadcrumb-item active"># {{ $almacen_personal->id }}</li>
                        </ul>
                    </div>
                </div>
            </div>
            {{-- message --}}
            {!! Toastr::message() !!}
            <!-- /Page Header -->
            <div class="card mb-0">
                <div class="card-body">

                    <div class="row">
                        <div class="col-md-5 d-flex">

                            <div class="card-body">
                                <h3 class="card-title">Informacion General</h3>
                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Tipo Negocio: </div>
                                        <div class="text-purple">
                                            {{ $almacen_personal->user->tipo_negocio }}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">Nombre: </div>
                                        <div class="text-purple">
                                            {{ $almacen_personal->user->nombre . ' ' . $almacen_personal->user->apellido_paterno . ' ' . $almacen_personal->user->apellido_materno }}
                                        </div>
                                    </li>
                                    <li>
                                        <div class="title">Producto: </div>
                                        <div class="text-purple">{{ $almacen_personal->producto->nombre }}</div>
                                    </li>
                                    <li>
                                        <div class="title">Capacidad: </div>
                                        <div class="text-purple">{{ $almacen_personal->producto->capacidad }}</div>
                                    </li>
                                    <li>
                                        <div class="title">Descripcion: </div>
                                        <div class="text-purple">{{ $almacen_personal->producto->descripcion }}</div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-md-6 d-flex">

                            <div class="card-body">
                                <h3 class="card-title">Informacion de Existencia</h3>
                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Estado: </div>
                                        <div class="text-info">{{ $almacen_personal->estado }}</div>
                                    </li>
                                    <li>
                                        <div class="title">Saldo Actual: </div>
                                        <div class="text-info">{{ $almacen_personal->cantidad_actual }}</div>
                                    </li>
                                    <li>
                                        <div class="title">Saldo Anterior: </div>
                                        <div class="text-info">{{ $almacen_personal->cantidad_anterior }}</div>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        {{-- <div class="col-sm-2">
                        <a href="#" class="btn btn-success" data-toggle="modal" data-target="#registrar_proveedor"><i class="fa fa-plus"></i>Fabrica</a>
                    </div> --}}
                        @if ($almacen_personal->user->tipo_negocio != 'Fabrica')
                            <div class="col-sm-2">
                                <a href="#" class="btn btn-info" data-toggle="modal"
                                    data-target="#registrar_vendedor"><i class="fa fa-plus"></i>Vendedores</a>
                            </div>
                        @endif

                        @if ($almacen_personal->user->tipo_negocio == 'Fabrica')
                            <div class="col-sm-2">
                                <a href="#" class="btn btn-success" data-toggle="modal"
                                    data-target="#registrar_transportista"><i class="fa fa-plus"></i>Transportista</a>
                            </div>
                        @endif

                        {{-- <div class="col-sm-2">
                        <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#vendedor"><i class="fa fa-plus"></i>Baja roto/rancio</a>
                    </div> --}}

                    </div>

                </div>
            </div>


            <div class="tab-content">
                <div class="tab-pane fade show active" id="bank_statutory">
                    <div class="card">
                        <div class="card-body">
                            <center>
                                <h3>KARDEX DE CONTROL FISICO</h3>
                            </center>
                            <table class="table table-striped custom-table datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th hidden>id</th>
                                        <th>Fecha</th>
                                        <th>Movimiento</th>
                                        <th>Vendedor</th>
                                        <th>Transportista</th>
                                        <th>DOC</th>
                                        <th>Cantidad</th>
                                        <th>Saldo</th>
                                        <th>Observaciones</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $x = 0;
                                    ?>
                                    @foreach ($historial_almacen_personales as $historial)
                                        <tr>
                                            <td hidden class="id">{{ $historial->id }}</td>
                                            <td class="fecha">{{ $historial->created_at }}</td>
                                            <td class="cliente_id">{{ $historial->movimiento }}</td>
                                            @if ($historial->users)
                                                <td class="producto_id">
                                                    {{ $historial->users->nombre . ' ' . $historial->users->apellido_paterno }}
                                                </td>
                                            @else
                                                <td class="producto_id">-------------</td>
                                            @endif
                                            @if ($historial->transportista)
                                                <td class="cantidad">
                                                    {{ $historial->transportista->nombre . ' ' . $historial->transportista->apellido_paterno }}
                                                </td>
                                            @else
                                                <td class="transportista2">-------------</td>
                                            @endif
                                            <td class="precio_unitario">{{ $historial->codigo }}</td>
                                            <td class="metodo_pago">{{ $historial->cantidad }}</td>
                                            <td class="total_venta">{{ $historial->saldo }}</td>
                                            <td class="total_venta">{{ $historial->obs }}</td>
                                            <td class="text-right">
                                                <?php
                                                $x++;
                                                ?>
                                                @can('borrar-registro-stok')
                                                    @if ($x == 1)
                                                        <a class="btn btn-danger btn-sm userDelete" href="#"
                                                            data-toggle="modal" ata-id="'.$historial->id.'"
                                                            data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                @endcan

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="registrar_proveedor" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrar Movimiento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('historialalmacen/add/save') }}" method="POST" onsubmit="bloquear()"
                            id="formulario" onsubmit="bloquear()">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $almacen_personal->id }}" id="almacen_id"
                                    name="almacen_id">
                                <div class="col-sm-6">
                                    <label class="col-form-label">Movimiento <span class="text-danger">*</span></label>
                                    <select required class="select" name="tipo">
                                        <option selected value="Ingreso">Ingreso</option>
                                        <option value="Salida de Envase">Salida de Envase</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="col-form-label">DOC<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="codigo"
                                            id="codigo">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <label class="col-form-label">Cantidad<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="number" class="form-control" name="cantidad"
                                            id="cantidad">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="col-form-label">Observaciones<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="obs" id="obs">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn"
                                    id="boton">Añadir</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div id="registrar_vendedor" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrar Movimiento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('historialalmacenpersonal/add/save') }}" method="POST"
                            onsubmit="bloquear()" id="formulario" onsubmit="bloquear()">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $almacen_personal->id }}" id="almacen_personal_id"
                                    name="almacen_personal_id">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Movimiento <span class="text-danger">*</span></label>
                                    <select required class="select" name="movimiento">
                                        <option selected value="Salida Ventas">Salida Ventas</option>
                                        <option value="Reingreso">Reingreso</option>
                                        <option value="Ingreso">Ingreso</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-form-label">Vendedor<span class="text-danger">*</span></label>
                                        <select class="select" name="user_id">
                                            <option required selected value=""> --Seleccione--</option>
                                            @foreach ($usuarios as $usuario)
                                                @if ($usuario->departamento->nombre == 'Ventas')
                                                    <option value="{{ $usuario->id }}">
                                                        {{ $usuario->nombre . ' ' . $usuario->apellido_paterno }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">DOC<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="codigo"
                                            id="codigo">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="col-form-label">Cantidad<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="number" class="form-control" name="cantidad"
                                            id="cantidad">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="col-form-label">Observaciones<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="obs" id="obs">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn"
                                    id="boton">Añadir</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>



        <div id="registrar_transportista" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Registrar Movimiento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('historialalmacenpersonal/transportista/add/save') }}" method="POST"
                            onsubmit="bloquear2()" id="formulario" onsubmit="bloquear()">
                            @csrf
                            <div class="row">
                                <input type="hidden" value="{{ $almacen_personal->id }}" id="almacen_personal_id"
                                    name="almacen_personal_id">
                                <div class="col-sm-4">
                                    <label class="col-form-label">Movimiento <span class="text-danger">*</span></label>
                                    <select required class="select" name="movimiento">
                                        <option selected value="Salida Ventas">Salida Ventas</option>
                                        <option value="Reingreso">Reingreso</option>
                                        <option value="Ingreso">Ingreso</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="col-form-label">Transportista<span
                                                class="text-danger">*</span></label>
                                        <select required class="select" name="transportista_id">
                                            <option selected disabled value=""> --Seleccione--</option>
                                            @foreach ($transportistas as $transportista)
                                                <option value="{{ $transportista->id }}">
                                                    {{ $transportista->nombre . ' ' . $transportista->apellido_paterno }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="col-form-label">DOC<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="text" class="form-control" name="codigo"
                                            id="codigo">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <label class="col-form-label">Cantidad<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input required type="number" class="form-control" name="cantidad"
                                            id="cantidad">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="col-form-label">Observaciones<span class="text-danger">*</span></label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="obs" id="obs">
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn"
                                    id="boton">Añadir</button>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




        <!-- Delete User Modal -->
        <div class="modal custom-modal fade" id="delete_user" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Eliminar Registro</h3>
                            <p>¿Estás seguro de que quieres eliminar el registro?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('almacenhispersonal/delete') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" class="e_id" value="">
                                <input type="hidden" name="almacen_personal_id" value="{{ $almacen_personal->id }}">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{-- delete js --}}
        <script>
            $(document).on('click', '.userDelete', function() {
                var _this = $(this).parents('tr');
                $('.e_id').val(_this.find('.id').text());
            });
        </script>
        {{-- delete js --}}
        <script>
            $(document).on('click', '.userDelete2', function() {
                var _this = $(this).parents('tr');
                $('.e_cuenta_id').val(_this.find('.cuenta_id').text());
                $('.e_hiscuenta_id').val(_this.find('.hiscuenta_id').text());
            });
        </script>


        <script>
            $(document).on('click', '.userDelete3', function() {
                var _this = $(this).parents('tr');

                $('.e_id').val(_this.find('.id').text());
            });
        </script>


        <script>
            function bloquear() {
                var btn = document.getElementById("boton");
                btn.disabled = true;
                $("#formulario :input").prop("readOnly", true);

            }
        </script>
        <script>
            function bloquear2() {
                var btn = document.getElementById("boton2");
                btn.disabled = true;
                $("#formulario2 :input").prop("readOnly", true);
            }
        </script>

        <script>
            function bloquear3() {
                var btn = document.getElementById("boton3");
                btn.disabled = true;
                $("#formulario3 :input").prop("readOnly", true);

            }
        </script>

        <script>
            function bloquear4() {
                var btn = document.getElementById("boton4");
                btn.disabled = true;
                $("#formulario4 :input").prop("readOnly", true);

            }
        </script>
    </div>
@endsection
