@extends('layouts.master')
@section('content')



    <!-- Page Wrapper -->
    <div class="page-wrapper">
        <!-- Page Content -->
        <div class="content container-fluid">
            <!-- Page Header -->
            <div class="page-header">
                <div class="row align-items-center">
                    <div class="col">
                        <h3 class="page-title">Administrador de Clientes</h3>
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ul>
                    </div>
                    <div class="col-auto float-right ml-auto">
                        @can('crear-cliente')
                            <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i
                                    class="fa fa-plus"></i> Añadir Cliente</a>
                        @endcan
                    </div>
                </div>
            </div>
            <!-- /Page Header -->


            {{-- message --}}
            {!! Toastr::message() !!}
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">

                            <table class="table table-striped custom-table datatable" style="width:100%">

                                <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Nombre</th>
                                        <th hidden>Nombre</th>
                                        <th hidden>Apellido Paterno</th>
                                        <th hidden>Apellido Materno</th>
                                        <th hidden>Carnet</th>
                                        <th hidden>Geolocalizacion</th>
                                        <th>Vendedor</th>
                                        <th>Codigo</th>
                                        <th hidden>Vendedor ID</th>
                                        <th>Tipo Negocio</th>
                                        <th>Telefono</th>
                                        <th>Zona</th>
                                        <th>Estado</th>
                                        <th hidden>Obs</th>
                                        <th>Tiempo Consumo</th>
                                        <th hidden>Fecha de Alta</th>
                                        <th class="text-right">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($clientes as $cliente)
                                        <tr>

                                            <td class="id">{{ $cliente->id }}</td>
                                            <td>
                                                <span hidden class="image">{{ $cliente->avatar }}</span>
                                                <h2 class="table-avatar">
                                                    <a class="avatar"> <img
                                                            src="{{ URL::to('/assets/images/' . $cliente->avatar) }}"
                                                            alt="{{ $cliente->avatar }}"></a>
                                                    {{ $cliente->nombre }} <br>
                                                    {{ $cliente->apellido_paterno }} <br>
                                                    {{ $cliente->pellido_materno }} <br>
                                                    </span></a>
                                                </h2>
                                            </td>


                                            <td hidden class="nombre">{{ $cliente->nombre }}</td>
                                            <td hidden class="apellido_paterno">{{ $cliente->apellido_paterno }}</td>
                                            <td hidden class="apellido_materno">{{ $cliente->apellido_materno }}</td>
                                            <td hidden class="carnet">{{ $cliente->carnet }}</td>
                                            <td hidden class="geolocalizacion">{{ $cliente->geolocalizacion }}</td>

                                            <td class="user_id">
                                                {{ $cliente->users->nombre . ' ' . $cliente->users->apellido_paterno }}</td>
                                            <td hidden class="user_id_id">{{ $cliente->users->id }}</td>

                                            <td class="codigo">{{ $cliente->codigo }}</td>

                                            <td class="tipo_negocio">{{ $cliente->tipo_negocio }}</td>


                                            <td class="telefono">{{ $cliente->telefono }}</td>
                                            <td class="direccion">{{ $cliente->zona }}</td>
                                            <td class="estado">{{ $cliente->estado }}</td>


                                            <td hidden class="obs">{{ $cliente->obs }}</td>

                                            <td class="tiempo_consumo_turril">{{ $cliente->tiempo_consumo_turril }}</td>
                                            <td hidden class="created_at">
                                                {{ \Carbon\Carbon::parse($cliente->created_at)->formatLocalized('%d de %B %Y') }}
                                            </td>



                                            <td class="text-right">

                                                @can('ver-perfil-cliente')
                                                    <a class="btn btn-success btn-sm"
                                                        href="{{ url('clientes/perfil/' . $cliente->id) }}"><i
                                                            class="fa fa-eye"></i></a>
                                                @endcan

                                                @can('editar-cliente')
                                                    <a class="btn btn-info btn-sm userUpdate" data-toggle="modal"
                                                        data-id="'.$cliente->id.'" data-target="#edit_user"><i
                                                            class="fa fa-pencil"></i></a>
                                                @endcan
                                                @can('borrar-cliente')
                                                    @if ($cliente->historial_cuaderno == '[]')
                                                        <a class="btn btn-danger btn-sm userDelete" href="#"
                                                            data-toggle="modal" ata-id="'.$user->id.'"
                                                            data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                                    @endif
                                                @endcan

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Page Content -->


        <!-- Add User Modal -->
        <div id="add_user" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Añadir Cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('cliente/add/save') }}" method="POST" enctype="multipart/form-data"
                            id="formulario" onsubmit="bloquear()">
                            @csrf
                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Asignar Vendedor
                                        <span style="color:red;">*</span>
                                    </label>
                                    <select required class="select" name="user_id" id="user_id">
                                        <option value=""> --Seleccione--</option>
                                        @foreach ($users as $usuario)
                                            @if ($usuario->departamento->nombre == 'Ventas' && $usuario->estado == 'Activo')
                                                <option value="{{ $usuario->id }}">
                                                    {{ $usuario->nombre . ' ' . $usuario->apellido_paterno }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nombres
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input class="form-control" type="text" id="nombre" name="nombre"
                                            placeholder="ej: Ana Maria">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Apellido Paterno</label>
                                        <input class="form-control" type="text" id="apellido_paterno"
                                            name="apellido_paterno" placeholder="ej: Condori">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Apellido Materno</label>
                                        <input class="form-control" type="text" id="apellido_materno"
                                            name="apellido_materno" placeholder="ej: Ramirez">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Carnet</label>
                                        <input class="form-control" type="text" id="carnet" name="carnet"
                                            placeholder="ej: 4578987 LP">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Telefono
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input class="form-control" min="10000000" max="99999999"
                                            type="number" id="telefono" name="telefono" placeholder="ej: 76569025">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Estado
                                            <span style="color:red;">*</span>
                                        </label>
                                        <select required class="select" name="estado" id="estado">
                                            <option value="Nuevo">Nuevo</option>
                                            <option value="Activo">Activo</option>
                                            <option value="Inactivo">Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Zona
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input required class="form-control" type="text" id="direccion"
                                            name="direccion" placeholder="ej: Villa adela">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Tiempo Cons Turr(en dias)</label>
                                        <input class="form-control" type="number" id="tiempo_consumo_turril"
                                            name="tiempo_consumo_turril" value="0">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-4">
                                    <label>Tipo de Negocio
                                        <span style="color:red;">*</span>
                                    </label>
                                    <select required class="select" name="tipo_negocio" id="tipo_negocio">
                                        <option value=""> --Seleccione--</option>
                                        <option value="Abarrotes">Abarrotes</option>
                                        <option value="Restaurante">Restaurante</option>
                                        <option value="Carniceria">Carniceria</option>
                                        <option value="Tienda">Tienda</option>
                                        <option value="Puesto movil">Puesto Movil</option>
                                        <option value="Kiosco">Kiosco</option>
                                        <option value="Feria">Feria</option>
                                        <option value="Empresa">Empresa</option>
                                        <option value="Mayorista">Mayorista</option>
                                        <option value="Fabrica">Fabrica</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Geolocalizacion</label>
                                        <input class="form-control" type="text" id="geolocalizacion"
                                            name="geolocalizacion">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Observaciones</label>
                                        <input class="form-control" type="text" id="obs" name="obs"
                                            placeholder="ej: Moroso">
                                    </div>
                                </div>
                            </div>

                            <a id="botonmap" href="#" onclick="someFunction()" class="btn btn-info">Mostrar
                                Mapa</a>
                            <div id="mapas" class="row" style="display: none;">
                                <div class="col-sm-12">
                                    <div id="map"></div>
                                </div>
                            </div>


                            <div class="submit-section">
                                <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Add User Modal -->

        <!-- Edit User Modal -->
        <div id="edit_user" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Editar Cliente</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <br>
                    <div class="modal-body">
                        <form action="{{ route('updatecliente') }}" enctype="multipart/form-data" method="POST"
                            id="formulario2" onsubmit="bloquear2()">
                            @csrf


                            <div class="row">
                                <div class="col-sm-4">
                                    <input type="hidden" name="id" id="e_id" value="">
                                    <label>Asignar Vendedor
                                        <span style="color:red;">*</span>
                                    </label>
                                    <select class="select" name="user_id" id="e_user_id">

                                        @foreach ($users as $usuario)
                                            @if ($usuario->departamento->nombre == 'Ventas' && $usuario->estado == 'Activo')
                                                <option value="{{ $usuario->id }}">
                                                    {{ $usuario->nombre . ' ' . $usuario->apellido_paterno }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Nombres
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input required class="form-control" type="text" id="e_nombre"
                                            name="nombre" placeholder="ej: Ana Maria">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Apellido Paterno</label>
                                        <input class="form-control" type="text" id="e_apellido_paterno"
                                            name="apellido_paterno" placeholder="ej: Condori">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Apellido Materno</label>
                                        <input class="form-control" type="text" id="e_apellido_materno"
                                            name="apellido_materno" placeholder="ej: Ramirez">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Carnet</label>
                                        <input class="form-control" type="text" id="e_carnet" name="carnet"
                                            placeholder="ej: 4578987 LP">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Telefono
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input class="form-control" type="number" id="e_telefono"
                                            min="10000000" max="99999999" name="telefono" placeholder="ej: 76569025">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Estado</label>
                                        <select required class="select" name="estado" id="e_estado">
                                            <option value="Nuevo">Nuevo</option>
                                            <option value="Activo">Activo</option>
                                            <option value="Inactivo">Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Zona
                                            <span style="color:red;">*</span>
                                        </label>
                                        <input required class="form-control" type="text" id="e_direccion"
                                            name="direccion" placeholder="ej: Villa adela">
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Tiempo Cons Turr(en dias)</label>
                                        <input class="form-control" type="number" id="e_tiempo_consumo_turril"
                                            name="tiempo_consumo_turril">
                                    </div>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-sm-4">
                                    <label>Tipo de Negocio
                                        <span style="color:red;">*</span>
                                    </label>
                                    <select class="select" name="tipo_negocio" id="e_tipo_negocio">
                                        <option value="Abarrotes">Abarrotes</option>
                                        <option value="Restaurante">Restaurante</option>
                                        <option value="Carniceria">Carniceria</option>
                                        <option value="Tienda">Tienda</option>
                                        <option value="Puesto movil">Puesto Movil</option>
                                        <option value="Kiosco">Kiosco</option>
                                        <option value="Feria">Feria</option>
                                        <option value="Empresa">Empresa</option>
                                        <option value="Mayorista">Mayorista</option>
                                        <option value="Fabrica">Fabrica</option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Geolocalizacion</label>
                                        <input class="form-control" type="text" id="e_geolocalizacion"
                                            name="geolocalizacion">
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label>Observaciones</label>
                                        <input class="form-control" type="text" id="e_obs" name="obs">
                                    </div>
                                </div>
                            </div>
                            <a id="botonmap2" href="#" onclick="someFunction2()" class="btn btn-info">Mostrar
                                Mapa</a>
                            <div id="mapas2" class="row" style="display: none;">
                                <div class="col-sm-12">
                                    <div id="map2"></div>
                                </div>
                            </div>
                            <div class="submit-section">
                                <button type="submit" id="boton2"
                                    class="btn btn-primary submit-btn">Actualizar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Edit Salary Modal -->

        <!-- Delete User Modal -->
        <div class="modal custom-modal fade" id="delete_user" role="dialog">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="form-header">
                            <h3>Eliminar Cliente</h3>
                            <p>¿Estás seguro de que quieres eliminar el registro?</p>
                        </div>
                        <div class="modal-btn delete-action">
                            <form action="{{ route('cliente/delete') }}" method="POST">
                                @csrf
                                <input type="hidden" name="id" class="e_id" value="">
                                <input type="hidden" name="avatar" class="e_avatar" value="">
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit"
                                            class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                    </div>
                                    <div class="col-6">
                                        <a href="javascript:void(0);" data-dismiss="modal"
                                            class="btn btn-primary cancel-btn">Cancelar</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Delete User Modal -->
    </div>
    <!-- /Page Wrapper -->
@section('script')
    {{-- update js --}}
    <script>
        $(document).on('click', '.userUpdate', function() {

            var _this = $(this).parents('tr');
            $('#e_id').val(_this.find('.id').text());
            $('#e_nombre').val(_this.find('.nombre').text());
            $('#e_apellido_paterno').val(_this.find('.apellido_paterno').text());
            $('#e_apellido_materno').val(_this.find('.apellido_materno').text());
            $('#e_carnet').val(_this.find('.carnet').text());
            $('#e_telefono').val(_this.find('.telefono').text());
            $('#e_direccion').val(_this.find('.direccion').text());
            $('#e_obs').val(_this.find('.obs').text());
            $("#e_geolocalizacion").val("");
            $('#e_geolocalizacion').val(_this.find('.geolocalizacion').text());
            $('#e_tiempo_consumo_turril').val(_this.find('.tiempo_consumo_turril').text());

            var user_id = (_this.find(".user_id").text());

            var user_id_id = (_this.find(".user_id_id").text());
            var _option1 = '<option selected value="' + user_id_id + '">' + _this.find('.user_id').text() +
                '</option>'
            $(_option1).appendTo("#e_user_id");

            var tipo_negocio = (_this.find(".tipo_negocio").text());
            var _option2 = '<option selected value="' + tipo_negocio + '">' + _this.find('.tipo_negocio').text() +
                '</option>'
            $(_option2).appendTo("#e_tipo_negocio");

            var estado = (_this.find(".estado").text());
            var _option3 = '<option selected value="' + estado + '">' + _this.find('.estado').text() + '</option>'
            $(_option3).appendTo("#e_estado");

            var x = document.getElementById("e_geolocalizacion").value;
            const myArray = x.split(",");
            if (x == '') {
                myArray[0] = -16.530204974734627;
                myArray[1] = -68.18154656093637;
            }

            const map = L.map('map2').setView([myArray[0], myArray[1]], 16);
            const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            }).addTo(map);
            var marker2 = new L.marker([myArray[0], myArray[1]], {
                draggable: true
            }).addTo(map);

            marker2.on('dragend', function(event) {
                var latlng = event.target.getLatLng();
                var inputNombre = document.getElementById("e_geolocalizacion");
                inputNombre.value = latlng.lat + ',' + latlng.lng;
            });
        });
    </script>

    {{-- delete js --}}
    <script>
        $(document).on('click', '.userDelete', function() {
            var _this = $(this).parents('tr');
            $('.e_id').val(_this.find('.id').text());
            $('.e_nombre').val(_this.find('.nombre').text());
            $('.e_foto_negocio').val(_this.find('.foto_negocio').text());
        });
    </script>

    <script>
        function bloquear() {
            var btn = document.getElementById("boton");
            $("#formulario :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>

    <script>
        function someFunction() {
            var div = $('#mapas');
            if (div.is(':visible')) {
                document.getElementById("mapas").style.display = "none";
                document.getElementById("botonmap").innerHTML = 'Mostrar Mapa';
            } else {
                document.getElementById("mapas").style.display = "block";
                document.getElementById("botonmap").innerHTML = 'Ocultar Mapa';
                setTimeout(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 1);
            }
        }
    </script>
    <script>
        function someFunction2() {
            var div = $('#mapas2');
            if (div.is(':visible')) {
                document.getElementById("mapas2").style.display = "none";
                document.getElementById("botonmap2").innerHTML = 'Mostrar Mapa';
            } else {
                document.getElementById("mapas2").style.display = "block";
                document.getElementById("botonmap2").innerHTML = 'Ocultar Mapa';
                setTimeout(function() {
                    window.dispatchEvent(new Event('resize'));
                }, 1);
            }
        }
    </script>
    <script>
        function bloquear2() {
            var btn = document.getElementById("boton2");
            $("#formulario2 :input").prop("readOnly", true);
            btn.disabled = true;
        }
    </script>

    <script>
        const map = L.map('map').setView([-16.530204974734627, -68.18154656093637], 16);
        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        var marker = new L.marker([-16.530204974734627, -68.18154656093637], {
            draggable: true
        }).addTo(map);

        marker.on('dragend', function(event) {
            var latlng = event.target.getLatLng();
            var inputNombre = document.getElementById("geolocalizacion");
            inputNombre.value = latlng.lat + ',' + latlng.lng;

        });
    </script>

    <script>
        marker2.on('dragend', function(event) {
            var latlng = event.target.getLatLng();
            var inputNombre = document.getElementById("e_geolocalizacion");
            inputNombre.value = latlng.lat + ',' + latlng.lng;
        });
    </script>
@endsection

@endsection
