@extends('layouts.master')
{{--
@section('menu')
@extends('sidebar.dashboard')
@endsection --}}
@section('content')


<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="page-title">Perfil de Usuarios</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="{{'/clientes' }}">Clientes</a></li>
                        <li class="breadcrumb-item active">{{$cliente->nombre.' '.$cliente->apellido_paterno}}</li>
                    </ul>
                </div>
            </div>
        </div>
        {{-- message --}}
        {!! Toastr::message() !!}
        <!-- /Page Header -->
        <div class="card mb-0">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="profile-view">
                            <div class="profile-img-wrap">
                                <div class="profile-img">
                                    <a href="#"><img alt="" src="{{ URL::to('/assets/images/'. $cliente->avatar) }}" alt="{{ $cliente->nombre }}"></a>
                                </div>
                            </div>
                            <div class="profile-basic">
                                <div class="row">
                                    <h3 class="user-name m-t-0 mb-0"><span class="text-danger">Nombre: </span>{{ $cliente->nombre.' '.$cliente->apellido_paterno.' '.$cliente->apellido_materno}}</h3>
                                </div><br>
                                <div class="row">

                                    <h4 class="user-name m-t-0 mb-0"><span class="text-danger">Vendedor: </span>{{ $cliente->users->nombre.' '.$cliente->users->apellido_paterno.' '.$cliente->users->apellido_materno}}</h4>
                                </div><br>
                                <div class="row">
                                    <h4 class="user-name m-t-0 mb-0"><span class="text-danger">Zona: </span>{{ $cliente->zona}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card tab-box">
            <div class="row user-tabs">
                <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
                    <ul class="nav nav-tabs nav-tabs-bottom">
                        <li class="nav-item"><a href="#emp_profile" data-toggle="tab" class="nav-link active">Perfil</a></li>
                        <li class="nav-item"><a href="#emp_projects" data-toggle="tab" class="nav-link">Compras</a></li>
                        <li class="nav-item"><a href="#bank_statutory" data-toggle="tab" class="nav-link">Extensible</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="tab-content">
            <!-- Profile Info Tab -->
            <div id="emp_profile" class="pro-overview tab-pane fade show active">
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Informacion Personal</h3>
                                <ul class="personal-info">
                                    <li>
                                        <div class="title">Nombres: </div>
                                        <div class="text-purple">{{$cliente->nombre}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Apellido Paterno:</div>
                                        <div class="text-purple">{{$cliente->apellido_paterno}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Apellido Materno:</div>

                                        @if($cliente->apellido_materno=='')
                                        <div class="text-purple"><br> </div>
                                        @else
                                        <div class="text-purple">{{$cliente->apellido_materno}}</div>
                                        @endif

                                        <div class="text-purple">{{$cliente->apellido_materno}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Carnet:</div>
                                        <div class="text-purple">{{$cliente->carnet}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Teléfono:</div>
                                        <div class="text-purple">{{$cliente->telefono}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Dirección</div>
                                        <div class="text-purple">{{$cliente->zona}}</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Información Comercial</h3>

                                <ul class="personal-info">
                                    <li>
                                        <div class="title">tipo de Negocio</div>
                                        <div class="text-info">{{$cliente->tipo_negocio}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Tiempo de consumo</div>
                                        <div class="text-info">{{$cliente->tiempo_consumo_turril}}</div>
                                    </li>
                                    <?php
                                    $total_compras = 0;
                                    ?>
                                    @foreach ($cliente->historial_cuaderno as $compra)
                                    <?php
                                    $total_compras = $total_compras + $compra->total_venta;
                                    ?>
                                    @endforeach

                                    <li>
                                        <div class="title">Total Compras</div>
                                        <div class="text-info">Bs. {{$total_compras}}</div>
                                    </li>

                                    <li>
                                        <div class="title">Estado</div>
                                        <div class="text-info">{{$cliente->estado}}</div>
                                    </li>

                                    <li>
                                        <div class="title">Creado</div>
                                        <div class="text-info">{{$cliente->created_at->diffForHumans()}}</div>
                                    </li>
                                    <li>
                                        <div class="title">Observaciones</div>
                                        <div class="text-info">{{$cliente->obs}}</div>
                                    </li>



                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Foto del Local</h3>
                                <div class="image">
                                    @if($cliente->foto_negocio=='')
                                    <img src="{{ URL::to('/assets/images/sin_data.jpg') }}" style="width:100%">
                                    @else
                                    <img src="{{ URL::to($cliente->foto_negocio) }}" style="width:100%">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Ubicacion en el Mapa</h3>
                                @if($cliente->geolocalizacion=='')
                                <div class="image">
                                    <img src="{{ URL::to('/assets/images/sin_data.jpg') }}" style="width:100%">
                                </div>
                                @else
                                <div id="map"></div>
                                <span hidden id="idTexto">{{$cliente->geolocalizacion}}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- /Profile Info Tab -->

            <!-- Projects Tab -->
            <div class="tab-pane fade" id="emp_projects">
                <div class="row">
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Compras Realizadas</h3>
                                <table class="table table-striped custom-table datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th hidden>id</th>
                                            <th>Cuaderno</th>
                                            <th>Monto</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cliente->historial_cuaderno as $compra)
                                        <tr>
                                            <td hidden class="id">{{ $compra->id }}</td>
                                            <td>{{ $compra->cuaderno->codigo }}</td>
                                            <td>{{ $compra->total_venta }}</td>
                                            <td>{{ $compra->created_at->diffForHumans()}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex">
                        <div class="card profile-box flex-fill">
                            <div class="card-body">
                                <h3 class="card-title">Deudas Pendientes</h3>
                                <table class="table table-striped custom-table datatable" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th hidden>id</th>
                                            <th>Monto</th>
                                            <th>Saldo</th>
                                            <th>Fecha Limite</th>
                                            <th class="text-right">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($cliente->historial_cuaderno as $compra)
                                        @foreach ($compra->cuenta as $cuenta)
                                        @if($cuenta->estado=='pendiente')
                                        <tr>
                                            <td hidden class="id">{{ $cuenta->id }}</td>
                                            <td>{{ $cuenta->saldo_inicial }}</td>
                                            <td>{{ $cuenta->saldo }}</td>
                                            <td>{{ date('d-m-Y',strtotime($cuenta->fecha_limite))}}</td>
                                            <td class="text-right">

                                                <a class="btn btn-success btn-sm" href="{{ url('cuentas/detalle/'.$cuenta->id) }}"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        @endif
                                        @endforeach
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Projects Tab -->

            <!-- Bank Statutory Tab -->

            <!-- /Bank Statutory Tab -->
        </div>
    </div>
    <!-- /Page Content -->
    <script>
        var x = document.getElementById('idTexto').innerHTML;
        var arr = x.split(',');
        const map = L.map('map').setView([arr[0], arr[1]], 16);
        const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);
        const marker = L.marker([arr[0], arr[1]]).addTo(map);
    </script>

    <!-- /Page Content -->
</div>
@endsection