@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Editar Rol</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item"><a href="/roles"> Roles</a></li>
                        <li class="breadcrumb-item active">Editar</li>
                        
                    </ul>
                </div>
                
            </div>
        </div>

        {!! Toastr::message() !!}
        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">

                            @if ($errors->any())
                            <div class="alert alert-dark alert-dismissible fade show" role="alert">
                                <strong>¡Revise los campos!</strong>
                                @foreach ($errors->all() as $error)
                                <span class="badge badge-danger">{{ $error }}</span>
                                @endforeach
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            @endif

                            {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}

                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label>Nombre del Rol</label>
                                        <input required class="form-control" type="text" id="name" name="name" value="{{$role->name}}" maxlength="50" minlength="3">
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        <label for="">Permisos para este Rol:</label>
                                        <br />
                                        <div class="row">
                                            @foreach($permission as $value)
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>{{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                                        {{ str_replace("-", " ",ucwords($value->name)) }}</label>
                                                    <br />
                                                </div>
                                            </div>

                                            @endforeach
                                        </div>
                                    </div>
                                </div>

                                <div class="submit-section">
                                    <button type="submit" class="btn btn-primary submit-btn">Actualizar</button>
                                </div>

                            </div>
                            
                            
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        

    </div>
    <!-- /Page Content -->
</div>

@endsection