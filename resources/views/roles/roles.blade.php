@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Roles</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Roles</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    @can('crear-rol')
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_user"><i class="fa fa-plus"></i> Añadir Rol</a>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /Page Header -->
        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Nombre del Rol</th>
                                    <th>Usuarios con Rol</th>
                                    <th>Fecha de Alta</th>
                                    <th class="text-right">Acción</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)

                                <?php
                                $contador = 0;
                                ?>
                                @foreach ($user as $usuario )
                                @if($usuario->roles()->first())
                                @if($role->id==$usuario->roles()->first()->id)
                                <?php
                                $contador = $contador + 1;
                                ?>
                                @endif
                                @endif

                                @endforeach


                                <tr>
                                    <td hidden class="id">{{ $role->id }}</td>
                                    <td class="name">{{ $role->name }}</td>
                                    <td class="usuarios_asignados">{{ $contador }}</td>
                                    <td class="created_at">{{date('d-m-Y',strtotime($role->created_at))}}</td>
                                    <td class="text-right">
                                        @can('editar-rol')
                                        <a class="btn btn-info btn-sm" href="{{ route('roles.edit',$role->id) }}"><i class="fa fa-pencil"></i></a>
                                        @endcan
                                        @can('borrar-rol')
                                        @if($contador==0)
                                        <a class="btn btn-danger btn-sm userDelete" href="#" data-toggle="modal" ata-id="'.$user->id.'" data-target="#delete_user"><i class="fa fa-trash"></i></a>
                                        @endif
                                        @endcan

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->


    <!-- Add User Modal -->
    <div id="add_user" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Añadir Rol</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('rol/add/save') }}" method="POST" id="formulario" onsubmit="bloquear()">
                        @csrf

                        <div class="form-group">
                            <label>Nombre del Rol <span style="color:red;">*</span></label>
                            <input required class="form-control" type="text" id="name" name="name" placeholder="ej: Supervisor" maxlength="50" minlength="3">
                        </div>
                        <div class="form-group">

                            <label for="">Permisos para este Rol:</label>
                            <br />
                            <div class="row">

                                @foreach($permission as $value)
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php
                                        $nombre_rol = explode("-", $value->name);
                                        ?>
                                        <label><input type="checkbox" name="permission[]" value="{{$value->id}}"> {{str_replace("-", " ",ucwords($value->name))}}</label><br>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                        <div class="submit-section">
                            <button type="submit" id="boton" class="btn btn-primary submit-btn">Añadir</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add User Modal -->

    <!-- Delete User Modal -->
    <div class="modal custom-modal fade" id="delete_user" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Eliminar Rol</h3>
                        <p>¿Estás seguro de que quieres eliminar el registro?</p>
                    </div>
                    <div class="modal-btn delete-action">
                        <form action="{{ route('rol/delete') }}" method="POST">
                            @csrf

                            <input type="hidden" name="id" class="e_id" value="">

                            <div class="row">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-primary continue-btn submit-btn">Eliminar</button>
                                </div>
                                <div class="col-6">
                                    <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-primary cancel-btn">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete User Modal -->
</div>
<!-- /Page Wrapper -->
@section('script')
{{-- update js --}}
<script>
    $(document).on('click', '.userUpdate', function() {
        var _this = $(this).parents('tr');
        $('#e_name').val(_this.find('.name').text());
        $('#e_id').val(_this.find('.id').text());
    });
</script>
{{-- delete js --}}
<script>
    $(document).on('click', '.userDelete', function() {
        var _this = $(this).parents('tr');
        $('.e_id').val(_this.find('.id').text());

    });
</script>

<script>
    function bloquear() {
        var btn = document.getElementById("boton");
        $("#formulario :input").prop("readOnly", true);
        btn.disabled = true;
    }
</script>

@endsection

@endsection