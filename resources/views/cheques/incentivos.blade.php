@extends('layouts.master')
@section('content')



<!-- Page Wrapper -->
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Administrador de Incentivos</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Inicio</a></li>
                        <li class="breadcrumb-item active">Incentivos</li>
                    </ul>
                </div>

            </div>
        </div>
        <!-- /Page Header -->

        <!-- Search Filter -->
        <form action="{{ route('incentivos/mes') }}" method="POST">
            @csrf
            <div class="row filter-row">
                <div class="col-sm-6 col-md-3">
                    <div class="form-group form-focus">
                        <input required type="month" name="fecha" id="fecha" class="form-control">

                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <button disabled type="sumit" class="btn btn-success btn-block"> Filtrar </button>
                </div>
            </div>
        </form>
        <!-- /Search Filter -->

        {{-- message --}}
        {!! Toastr::message() !!}
        <div class="row">
            <div class="col-md-12">
                <center>
                    <h3>VENTAS MENSUALES DE VENDEDORES</h3>
                </center>
                <div class="row staff-grid-row">
                    @foreach ($vendedores as $vendedor )
                    <?php
                    $vendido = 0;
                    ?>
                    @foreach($vendedor->cuaderno as $cuaderno)
                    <?php
                    $vendido = $vendido + $cuaderno->total_venta;
                    ?>
                    @endforeach
                    <div class="col-md-4 col-sm-6 col-12 col-lg-4 col-xl-3">
                        <div class="profile-widget">
                            <div class="profile-img">
                                <a class="avatar"><img src="{{ URL::to('/assets/images/'. $vendedor->avatar) }}"></a>
                            </div>
                            @if($vendido>0)
                            <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a class="text-success">Bs. {{ $vendido}}</a></h4>
                            @else
                            <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a class="text-danger">Bs. {{ $vendido}}</a></h4>
                            @endif
                            <div class="small text-muted">{{ $vendedor->nombre.' '.$vendedor->apellido_paterno }}</div>
                            <?php
                            $pro = 0;
                            ?>
                            <br>

                            @foreach($productos as $producto)
                            <?php
                            $pro = 0;
                            ?>
                            @foreach($vendedor->cuaderno as $cuaderno)
                            @foreach($cuaderno->historial_cuaderno as $his)
                            @if($his->producto_id==$producto->id)
                            <?php
                            $pro = $pro+$his->cantidad;
                            ?>
                            @endif
                            @endforeach
                            @endforeach
                            <h4 class="user-name m-t-10 mb-0 text-ellipsis"><a class="text-purple">{{$producto->nombre.': '.$pro}}</a></h4>
                            @endforeach
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped custom-table datatable" style="width:100%">
                            <thead>
                                <tr>
                                    <th hidden>id</th>
                                    <th>Vendedor</th>
                                    <th>Mes</th>
                                    <th>Venta Total</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

</div>
<!-- /Page Wrapper -->
@section('script')

@endsection

@endsection