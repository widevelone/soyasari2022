<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
	<meta name="description" content="Distribuidora Soya Sari">

	<title>Soya Sari</title>
	<!-- Favicon -->
	<link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('assets/img/favicon.png') }}">
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap.min.css') }}">
	<!-- Fontawesome CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/font-awesome.min.css') }}">
	<!-- Lineawesome CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/line-awesome.min.css') }}">

	<!--datables CSS básico-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('assets/data/datatables.min.css') }}">
	<!--datables estilo bootstrap 4 CSS-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('assets/data/DataTables-1.12.1/css/dataTables.bootstrap4.min.css') }}">


	<!-- Datatable CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/dataTables.bootstrap4.min.css') }}">
	<!-- Select2 CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/select2.min.css') }}">
	<!-- Datetimepicker CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/bootstrap-datetimepicker.min.css') }}">
	<!-- Chart CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/plugins/morris/morris.css') }}">
	<!-- Main CSS -->
	<link rel="stylesheet" href="{{ URL::to('assets/css/style.css') }}">
	{{-- message toastr --}} <link rel="stylesheet" href="{{ URL::to('assets/css/toastr.min.css') }}">
		<script src="{{ URL::to('assets/js/toastr_jquery.min.js') }}"></script>
		<script src="{{ URL::to('assets/js/toastr.min.js') }}"></script>


		<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.2/dist/leaflet.css" integrity="sha256-sA+zWATbFveLLNqWO2gtiw3HL/lh1giY/Inf1BJ0z14=" crossorigin="" />
		<script src="https://unpkg.com/leaflet@1.9.2/dist/leaflet.js" integrity="sha256-o9N1jGDZrf5tS+Ft4gbIK7mYMipq9lqpVJ91xHSyKhg=" crossorigin=""></script>

		<style>
			.leaflet-container {
				height: 400px;
				width: 800px;
				max-width: 100%;
				max-height: 100%;
			}
		</style>
</head>

<body>
	<style>
		.invalid-feedback {
			font-size: 14px;
		}
	</style>
	<!-- Main Wrapper -->
	<div class="main-wrapper">


		<!-- Loader -->
		<div id="loader-wrapper">
			<div id="loader">
				<div class="loader-ellips">
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
					<span class="loader-ellips__dot"></span>
				</div>
			</div>
		</div>
		<!-- /Loader -->






		<!-- Header -->
		{{-- @yield('nav') --}}
		<div class="header">
			<!-- Logo -->
			<div class="header-left">
				<a href="{{ route('home') }}" class="logo"> <img src="{{ URL::to('assets/img/logo2.png') }}" width="150" height="55" alt=""> </a>
			</div>
			<!-- /Logo -->
			<a id="toggle_btn" href="javascript:void(0);">
				<span class="bar-icon"><span></span><span></span><span></span></span>
			</a>
			<!-- Header Title -->
			<div class="page-title-box">
				<h3>{{ Auth::user()->departamento->nombre}} - {{Auth::user()->roles->first()->name}}</h3>
			</div>
			<!-- /Header Title -->
			<a id="mobile_btn" class="mobile_btn" href="#sidebar"><i class="fa fa-bars"></i></a>
			<!-- Header Menu -->
			<ul class="nav user-menu">
				<!-- Search 
				<li class="nav-item">
					<div class="top-nav-search">
						<a href="javascript:void(0);" class="responsive-search"> <i class="fa fa-search"></i> </a>
						<form action="search.html">
							<input class="form-control" type="text" placeholder="Buscar">
							<button class="btn" type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</li>
				Search -->

				<!-- Notifications -->
				<li class="nav-item dropdown">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"> <i class="fa fa-bell-o"></i> <span class="badge badge-pill">3</span> </a>
					<div class="dropdown-menu notifications">
						<div class="topnav-dropdown-header"> <span class="notification-title">Notificaciones</span> <a href="javascript:void(0)" class="clear-noti"> Limpiar Todo </a> </div>
						<div class="noti-content">
							<ul class="notification-list">
								<li class="notification-message">
									<a href="activities.html">
										<div class="media"> <span class="avatar">
												<img alt="" src="{{ URL::to('assets/images/chica.png') }}">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Maria Lopez</span> realizó 1 nuevo pedido <span class="noti-title"> para el dia viernes</span></p>
												<p class="noti-time"><span class="notification-time">hace 3 horas</span></p>
											</div>
										</div>
									</a>
								</li>
								<li class="notification-message">
									<a href="activities.html">
										<div class="media"> <span class="avatar">
												<img alt="" src="{{ URL::to('assets/images/chico.png') }}">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Juan Aguilar</span> tiene una deuda que vence <span class="noti-title">en 2 dias</span></p>
												<p class="noti-time"><span class="notification-time">hace 5 minutos</span></p>
											</div>
										</div>
									</a>
								</li>
								<li class="notification-message">
									<a href="activities.html">
										<div class="media"> <span class="avatar">
												<img alt="" src="{{ URL::to('assets/images/chico.png') }}">
											</span>
											<div class="media-body">
												<p class="noti-details"><span class="noti-title">Juan Gutierrez</span> ha sido inhabilitado por <span class="noti-title">Mario Torrez</span></p>
												<p class="noti-time"><span class="notification-time">hace 15 segundos</span></p>
											</div>
										</div>
									</a>
								</li>

							</ul>
						</div>
						<div class="topnav-dropdown-footer"> <a href="activities.html">Ver todas las Notificaciones</a> </div>
					</div>
				</li>
				<!-- /Notifications -->
				<!-- Message Notifications -->
				<li class="nav-item dropdown">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown"> <i class="fa fa-comment-o"></i> <span class="badge badge-pill">4</span> </a>
					<div class="dropdown-menu notifications">
						<div class="topnav-dropdown-header"> <span class="notification-title">Mensajes</span> <a href="javascript:void(0)" class="clear-noti"> Limpiar Todo </a> </div>
						<div class="noti-content">
							<ul class="notification-list">
								<li class="notification-message">
									<a href="chat.html">
										<div class="list-item">
											<div class="list-left"> <span class="avatar">
													<img alt="" src="{{ URL::to('assets/images/chico.png') }}">
												</span> </div>
											<div class="list-body"> <span class="message-author">Carlos Rodriguez </span> <span class="message-time">12:28 AM</span>
												<div class="clearfix"></div> <span class="message-content">Buen dia, le solicito licencia para mañana por motivo de salud</span>
											</div>
										</div>
									</a>
								</li>
								<li class="notification-message">
									<a href="chat.html">
										<div class="list-item">
											<div class="list-left"> <span class="avatar">
													<img alt="" src="{{ URL::to('assets/images/chica.png') }}">
												</span> </div>
											<div class="list-body"> <span class="message-author">Maria Lopez</span> <span class="message-time">6 Mar</span>
												<div class="clearfix"></div> <span class="message-content">Los informes mensuales estan listos</span>
											</div>
										</div>
									</a>
								</li>
								<li class="notification-message">
									<a href="chat.html">
										<div class="list-item">
											<div class="list-left"> <span class="avatar">
													<img alt="" src="{{ URL::to('assets/images/chica.png') }}">
												</span> </div>
											<div class="list-body"> <span class="message-author">Amanda Aliaga </span> <span class="message-time">5 Mar</span>
												<div class="clearfix"></div> <span class="message-content">Se necesita autorizacion para realizar la compra de</span>
											</div>
										</div>
									</a>
								</li>
								<li class="notification-message">
									<a href="chat.html">
										<div class="list-item">
											<div class="list-left"> <span class="avatar">
													<img alt="" src="{{ URL::to('assets/images/chico.png') }}">
												</span> </div>
											<div class="list-body"> <span class="message-author">Roberto Mamani</span> <span class="message-time">3 Mar</span>
												<div class="clearfix"></div> <span class="message-content">Confimo mi asistencia para la reunión de hoy</span>
											</div>
										</div>
									</a>
								</li>

							</ul>
						</div>
						<div class="topnav-dropdown-footer"> <a href="chat.html">Ver todos los Mensajes</a> </div>
					</div>
				</li>
				<!-- /Message Notifications -->
				<li class="nav-item dropdown has-arrow main-drop">
					<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
						<span class="user-img">
							<img src="{{ URL::to('/assets/images/'. Auth::user()->avatar) }}" alt="{{ Auth::user()->nombre }}">
							<span class="status online"></span></span>
						<span>{{ Auth::user()->nombre }}</span>
					</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="{{ url('usuarios/perfil/'.Auth::user()->id) }}">Mi perfil</a>
						<a class="dropdown-item" href="/perfil/contraseña">Contraseña</a>
						<a class="dropdown-item" href="{{ route('logout') }}">Cerrar Sesión</a>
					</div>
				</li>
			</ul>
			<!-- /Header Menu -->

		</div>
		<div class="sidebar" id="sidebar">
			<div class="sidebar-inner slimscroll">
				<div id="sidebar-menu" class="sidebar-menu">
					<ul>


						<li class="{{set_active(['home'])}}">
							<a href="/home" class="{{ set_active(['home']) ? 'noti-dot' : '' }}">
								<i class="fa fa-home"></i></i> <span>Inicio</span></a>
						</li>
						@canany(['ver-rol', 'ver-departamento', 'ver-usuario','ver-producto'])
						<li class="menu-title">
							<span>Administración</span>
						</li>
						@endcanany
						@can('ver-rol')
						<li class="{{set_active(['roles'])}}">
							<a href="/roles" class="{{ set_active(['roles']) ? 'noti-dot' : '' }}">
								<i class="fa fa-check-square-o"></i></i> <span>Roles y Permisos</span></a>
						</li>
						@endcan
						@can('ver-departamento')
						<li class="{{set_active(['departamentos'])}}">
							<a href="/departamentos" class="{{ set_active(['departamentos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-briefcase"></i></i> <span>Departamentos</span></a>
						</li>
						@endcan
						@can('ver-usuario')
						<li class="{{set_active(['usuarios'])}}">
							<a href="/usuarios" class="{{ set_active(['usuarios']) ? 'noti-dot' : '' }}">
								<i class="fa fa-users"></i></i> <span>Usuarios</span></a>
						</li>
						@endcan

						@can('ver-producto')
						<li class="{{set_active(['productos'])}}">
							<a href="/productos" class="{{ set_active(['productos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-th"></i></i> <span>Productos</span></a>
						</li>
						@endcan
						@canany(['ver-vendedores', 'ver-cliente', 'ver-cuaderno'])
						<li class="menu-title">
							<span>Ventas</span>
						</li>
						@endcanany
						@can('ver-vendedores')
						<li class="{{set_active(['vendedores'])}}">
							<a href="/vendedores" class="{{ set_active(['vendedores']) ? 'noti-dot' : '' }}">
								<i class="fa fa-users"></i></i> <span>Vendedores</span></a>
						</li>
						@endcan

						@can('ver-cliente')
						<li class="{{set_active(['clientes'])}}">
							<a href="/clientes" class="{{ set_active(['clientes']) ? 'noti-dot' : '' }}">
								<i class="fa fa-handshake-o"></i></i> <span>Clientes</span></a>
						</li>
						@endcan

						@can('ver-cuaderno')
						<li class="{{set_active(['cuadernos'])}}">
							<a href="/cuadernos" class="{{ set_active(['cuadernos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-shopping-cart"></i></i> <span>Cuadernos</span></a>
						</li>
						@endcan

						@canany(['ver-prestamos-y-garantias', 'ver-deuda', 'ver-transferencias','ver-gasto','ver-incentivo'])
						<li class="menu-title">
							<span>Contabilidad</span>
						</li>
						@endcanany
						@can('ver-incentivo')
						<li class="{{set_active(['incentivos'])}}">
							<a href="/incentivos" class="{{ set_active(['incentivos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-arrow-circle-o-up"></i></i> <span>Incentivos</span></a>
						</li>
						@endcan
						@can('ver-deuda')
						<li class="{{set_active(['cuentas'])}}">
							<a href="/cuentas" class="{{ set_active(['cuentas']) ? 'noti-dot' : '' }}">
								<i class="fa fa-ticket"></i></i> <span>Deudas</span></a>
						</li>
						@endcan
						@can('ver-prestamos')
						<li class="{{set_active(['prestamos'])}}">
							<a href="/prestamos" class="{{ set_active(['prestamos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-cubes"></i></i> <span>Prestamos</span></a>
						</li>
						@endcan
						@can('ver-garantias')
						<li class="{{set_active(['garantias'])}}">
							<a href="/garantias" class="{{ set_active(['garantias']) ? 'noti-dot' : '' }}">
								<i class="fa fa-cube"></i></i> <span>Garantias</span></a>
						</li>
						@endcan
						@can('ver-gasto')
						<li class="{{set_active(['gastos'])}}">
							<a href="/gastos" class="{{ set_active(['gastos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-address-book-o"></i></i> <span>Gastos</span></a>
						</li>
						@endcan
						@can('ver-transferencias')
						<li class="{{set_active(['transferencias'])}}">
							<a href="/transferencias" class="{{ set_active(['transferencias']) ? 'noti-dot' : '' }}">
								<i class="fa fa-credit-card"></i></i> <span>Transferencias</span></a>
						</li>
						@endcan



						@can('ver-almacen')
						<li class="menu-title">
							<span>Alamacenes</span>
						</li>

						<li class="{{set_active(['almacenes'])}}">
							<a href="/almacenes" class="{{ set_active(['almacenes']) ? 'noti-dot' : '' }}">
								<i class="fa fa-shopping-cart"></i></i> <span>Almacenes</span></a>
						</li>

						<li class="{{set_active(['transportistas'])}}">
							<a href="/transportistas" class="{{ set_active(['transportistas']) ? 'noti-dot' : '' }}">
								<i class="fa fa-car"></i></i> <span>Transportistas</span></a>
						</li>

						<li class="{{set_active(['almacenes_personales'])}}">
							<a href="/almacenes_personales" class="{{ set_active(['almacenes_personales']) ? 'noti-dot' : '' }}">
								<i class="fa fa-shopping-cart"></i></i> <span>Almacenes<br />personales</span></a>
						</li>
						@endcan


						{{-- @can(['ver-descuento']) --}}
						<li class="menu-title">
							<span>Tesoreria</span>
						</li>
						
						<li class="{{set_active(['tesoreria/descuentos'])}}">
							<a href="/tesoreria/descuentos" class="{{ set_active(['tesoreria/descuentos']) ? 'noti-dot' : '' }}">
								<i class="fa fa-shopping-cart"></i></i> <span>Descuentos</span></a>
						</li>

						<li class="{{set_active(['tesoreria/pago/pago_ref_pas_carros'])}} submenu">
							<a href="#" class="{{ set_active(['tesoreria/pago/pago_ref_pas_carros']) ? 'noti-dot' : '' }}">
								<i class="la la-user"></i>
								<span>Pagos</span> <span class="menu-arrow"></span>
							</a>
							<ul>

								<!-- <li><a class="{{set_active(['registro/sesiones'])}}" href="/registro/sesiones">Servicios</a></li> -->

								<li><a class="{{set_active(['tesoreria/pago/pago_ref_pas_carros'])}}" href="/tesoreria/pago/pago_ref_pas_carros">Refrigerio - Pasaje</a></li>

							</ul>
						</li>

						{{-- @endcan --}}


						@can('ver-registro')
						<li class="menu-title">
							<span>Seguridad</span>
						</li>
						<li class="{{set_active(['registro/sesiones','registro/cambios'])}} submenu">
							<a href="#" class="{{ set_active(['registro/sesiones','registro/cambios']) ? 'noti-dot' : '' }}">
								<i class="la la-user"></i>
								<span>Registros</span> <span class="menu-arrow"></span>
							</a>
							<ul>

								<li><a class="{{set_active(['registro/sesiones'])}}" href="/registro/sesiones">Sesiones</a></li>

								<li><a class="{{set_active(['registro/cambios'])}}" href="/registro/cambios">Cambios</a></li>

							</ul>
						</li>
						@endcan

					</ul>
				</div>
			</div>
		</div>
		<!-- /Header -->
		<!-- Sidebar -->
		{{-- @yield('menu') --}}
		<!-- /Sidebar -->
		<!-- Page Wrapper -->
		@yield('content')
		<!-- /Page Wrapper -->
	</div>
	<!-- /Main Wrapper -->

	<!-- jQuery -->
	<script src="{{ URL::to('assets/js/jquery-3.5.1.min.js') }}"></script>
	<!-- Bootstrap Core JS -->
	<script src="{{ URL::to('assets/js/popper.min.js') }}"></script>
	<script src="{{ URL::to('assets/js/bootstrap.min.js') }}"></script>
	<!-- Chart JS -->
	<script src="{{ URL::to('assets/plugins/morris/morris.min.js') }}"></script>
	<script src="{{ URL::to('assets/plugins/raphael/raphael.min.js') }}"></script>
	<script src="{{ URL::to('assets/js/chart.js') }}"></script>
	<!-- Slimscroll JS -->
	<script src="{{ URL::to('assets/js/jquery.slimscroll.min.js') }}"></script>
	<!-- Select2 JS -->
	<script src="{{ URL::to('assets/js/select2.min.js') }}"></script>
	<!-- Datetimepicker JS -->
	<script src="{{ URL::to('assets/js/moment.min.js') }}"></script>
	<script src="{{ URL::to('assets/js/bootstrap-datetimepicker.min.js') }}"></script>
	<!-- Datatable JS -->
	<script src="{{ URL::to('assets/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ URL::to('assets/js/dataTables.bootstrap4.min.js') }}"></script>

	<!-- para usar botones en datatables JS -->
	<script src="{{ URL::to('assets/data/Buttons-2.2.3/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ URL::to('assets/data/JSZip-2.5.0/jszip.min.js') }}"></script>
	<script src="{{ URL::to('assets/data/pdfmake-0.1.36/pdfmake.min.js') }}"></script>
	<script src="{{ URL::to('assets/data/pdfmake-0.1.36/vfs_fonts.js') }}"></script>
	<script src="{{ URL::to('assets/data/Buttons-2.2.3/js/buttons.html5.min.js') }}"></script>

	<script type="text/javascript" src="{{ URL::to('assets/data/datatables.min.js') }}"></script>


	<!-- Multiselect JS -->
	<script src="{{ URL::to('assets/js/multiselect.min.js') }}"></script>
	<!-- Custom JS -->
	<script src="{{ URL::to('assets/js/app.js') }}"></script>

	@yield('script')
</body>

</html>