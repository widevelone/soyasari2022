<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\RolController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\CuadernoController;
use App\Http\Controllers\HistorialcuadernoController;
use App\Http\Controllers\CuentaController;
use App\Http\Controllers\ChequesController;
use App\Http\Controllers\AlmacenController;
use App\Http\Controllers\AlmacenPersonalController;
use App\Http\Controllers\DescuentoPVController;
use App\Http\Controllers\PagosRPCVController;
use Illuminate\Support\Facades\Request;

Route::get('/', function () {
        return view('auth.login');
});


Route::group(['middleware' => 'auth'], function () {

    Route::resource('almacenes', AlmacenController::class);
    Route::post('almacen/add/save', [App\Http\Controllers\AlmacenController::class, 'addNewAlmacenSave'])->name('almacen/add/save');
    Route::get('almacenes/registrar/{id}', [App\Http\Controllers\AlmacenController::class, 'registrarAlmacen'])->name('almacenes.registrar');
    Route::post('historialalmacen/add/save', [App\Http\Controllers\AlmacenController::class, 'addNewHistorialalmacenSave'])->name('historialalmacen/add/save');
    Route::post('almacenhis/delete', [App\Http\Controllers\AlmacenController::class, 'deletehis'])->middleware('auth')->name('almacenhis/delete');
    Route::post('almacen/delete', [App\Http\Controllers\AlmacenController::class, 'delete'])->middleware('auth')->name('almacen/delete');
    
    Route::resource('almacenes_personales', AlmacenPersonalController::class);
    Route::post('almacen_personal/add/save', [App\Http\Controllers\AlmacenPersonalController::class, 'addNewAlmacenPersonalSave'])->name('almacen_personal/add/save');
    Route::post('almacen_personal/delete', [App\Http\Controllers\AlmacenPersonalController::class, 'delete'])->middleware('auth')->name('almacen_personal/delete');
    Route::get('almacenes_personales/registrar/{id}', [App\Http\Controllers\AlmacenPersonalController::class, 'registrarAlmacenPersonal'])->name('almacenes_personales.registrar');
    
    Route::post('almacenhispersonal/delete', [App\Http\Controllers\AlmacenPersonalController::class, 'deletehis'])->middleware('auth')->name('almacenhispersonal/delete');
    Route::post('historialalmacenpersonal/add/save', [App\Http\Controllers\AlmacenPersonalController::class, 'addNewHistorialalmacenPersonalSave'])->name('historialalmacenpersonal/add/save');
    Route::post('historialalmacenpersonal/transportista/add/save', [App\Http\Controllers\AlmacenPersonalController::class, 'addNewHistorialalmacenPersonalTransportistaSave'])->name('historialalmacenpersonal/transportista/add/save');
    
    // descuentos
    Route::resource('tesoreria/descuentos', DescuentoPVController::class);
    Route::post('descuentopv/add/save', [App\Http\Controllers\DescuentoPVController::class, 'addNew'])->name('descuentopv/add/save');
    Route::get('tesoreria/historialdescuentos/{id}', [App\Http\Controllers\DescuentoPVController::class, 'historialDescuentos'])->name('tesoreria/descuentos.historialdescuento');
    Route::post('historialdesc/add/save', [App\Http\Controllers\DescuentoPVController::class, 'addNewHistorialDescuentos'])->name('historialdesc/add/save');
    Route::post('historialdesc/delete', [App\Http\Controllers\DescuentoPVController::class, 'removeNewHistorialDescuentos'])->name('historialdesc/delete');
    Route::post('descuento/cerrar', [App\Http\Controllers\DescuentoPVController::class, 'descuentoCerrar'])->name('descuento/cerrar');
    Route::post('descuento/delete', [App\Http\Controllers\DescuentoPVController::class, 'removeDescuento'])->name('descuento/delete');
    
    //pagos-refrigerios-pasajer-carros
    Route::resource('tesoreria/pago/pago_ref_pas_carros', PagosRPCVController::class);
    Route::post('pagorpc/add/save', [App\Http\Controllers\PagosRPCVController::class, 'addNew'])->name('pagorpc/add/save');
    Route::get('tesoreria/his_pago_ref_pas_carros/{id}', [App\Http\Controllers\PagosRPCVController::class, 'historialPagorpc'])->name('tesoreria/pago/pago_ref_pas_carros.his_pago_ref_pas_carros');
    Route::post('pagorpc/his/add/save', [App\Http\Controllers\PagosRPCVController::class, 'addNewPagorpc'])->name('pagorpc/his/add/save');
    Route::post('pagorpc/his/delete', [App\Http\Controllers\PagosRPCVController::class, 'removeHistorialPagosPcv'])->name('pagorpc/his/delete');
    Route::post('pagorpc/cerrar', [App\Http\Controllers\PagosRPCVController::class, 'pagoPcvCerrar'])->name('pagorpc/cerrar');

    

    


    Route::get('transportistas', [App\Http\Controllers\AlmacenController::class, 'transportistas'])->name('transportistas');
    Route::post('trans/add/save', [App\Http\Controllers\AlmacenController::class, 'addNewTransSave'])->name('trans/add/save');
    Route::post('updatetrans', [App\Http\Controllers\AlmacenController::class, 'updatetrans'])->name('updatetrans');
    Route::post('trans/delete', [App\Http\Controllers\AlmacenController::class, 'deletetrans'])->name('trans/delete');

    Route::resource('cuadernos', CuadernoController::class);
    Route::post('cuaderno/add/save', [App\Http\Controllers\CuadernoController::class, 'addNewCuadernoSave'])->name('cuaderno/add/save');
    Route::get('cuadernos/registrar/{id}', [App\Http\Controllers\HistorialCuadernoController::class, 'registrarCuaderno'])->name('cuadernos.registrar');
    Route::post('updatecuaderno', [App\Http\Controllers\CuadernoController::class, 'updatecuaderno'])->name('updatecuaderno');
    Route::post('cuaderno/created_at/update', [App\Http\Controllers\CuadernoController::class, 'updateCreated_at'])->name('cuaderno/created_at/update');

 
    Route::resource('historial', HistorialcuadernoController::class);
    Route::post('historial/add/save', [App\Http\Controllers\HistorialCuadernoController::class, 'addNewHistorialSave'])->name('historial/add/save');
    Route::post('gasto/add/save', [App\Http\Controllers\HistorialCuadernoController::class, 'addNewGastoSave'])->name('gasto/add/save');

    Route::post('efectivo/add/save', [App\Http\Controllers\HistorialCuadernoController::class, 'addNewEfectivoSave'])->name('efectivo/add/save');

    Route::post('turril/add/save', [App\Http\Controllers\HistorialCuadernoController::class, 'addNewTurrilSave'])->name('turril/add/save');
    
    Route::post('historial/delete', [App\Http\Controllers\HistorialCuadernoController::class, 'deletehis'])->name('his/delete');

    Route::post('gasto/delete', [App\Http\Controllers\HistorialCuadernoController::class, 'deletegasto'])->name('gasto/delete');

    Route::post('cuaderno/cerrar', [App\Http\Controllers\HistorialCuadernoController::class, 'cerrar'])->name('cuaderno/cerrar');


    Route::post('/productoDeVenta', [App\Http\Controllers\CuadernoController::class, 'agregarProductoVenta'])->name('agregarProductoVenta');
    Route::delete('/productoDeVenta', [App\Http\Controllers\CuadernoController::class, 'quitarProductoDeVenta'])->name("quitarProductoDeVenta");
    Route::post('/terminarOCancelarVenta', [App\Http\Controllers\CuadernoController::class, 'terminarOCancelarVenta'])->name("terminarOCancelarVenta");

    Route::resource('cuentas', CuentaController::class);
    Route::get('gastos', [App\Http\Controllers\CuentaController::class, 'gastos'])->name('gastos');
    Route::get('incentivos', [App\Http\Controllers\CuentaController::class, 'incentivos'])->name('incentivos');
    Route::post('incentivos/mes', [App\Http\Controllers\CuentaController::class, 'searchUser'])->name('incentivos/mes');
    Route::get('prestamos', [App\Http\Controllers\CuentaController::class, 'prestamos'])->name('prestamos');
    Route::get('garantias', [App\Http\Controllers\CuentaController::class, 'garantias'])->name('garantias');
    Route::post('cuentas/add/save', [App\Http\Controllers\CuentaController::class, 'addNewCuentaSave'])->name('cuenta/add/save');
    Route::post('anterior/add/save', [App\Http\Controllers\CuentaController::class, 'addNewAnteriorSave'])->name('anterior/add/save');
    Route::get('cuentas/detalle/{id}', [App\Http\Controllers\CuentaController::class, 'ver_cuenta'])->middleware('auth');
    Route::get('registrar-anteriores', [App\Http\Controllers\CuentaController::class, 'registraranteriores'])->name('registrar-anteriores');
    Route::post('cuentas/fecha_limite/update', [App\Http\Controllers\CuentaController::class, 'updateFechaLimite'])->name('cuentas/fecha_limite/update');
    Route::post('prestamos/fecha_limite/update', [App\Http\Controllers\CuentaController::class, 'updateFechaLimitePrestamoTurriles'])->name('prestamos/fecha_limite/update');
    


    Route::post('hiscuentas/add/save', [App\Http\Controllers\HistorialCuentaController::class, 'addNewhisCuentaSave'])->name('hiscuenta/add/save');
   
    Route::post('hiscuenta/delete', [App\Http\Controllers\HistorialCuentaController::class, 'deletehiscuenta'])->name('hiscuenta/delete');
    Route::post('updatepagocuenta', [App\Http\Controllers\ChequesController::class, 'updatepagocuenta'])->name('updatepagocuenta');


    Route::resource('roles', RolController::class);
    Route::post('rol/add/save', [App\Http\Controllers\RolController::class, 'addNewRolSave'])->name('rol/add/save');
    Route::post('updaterol', [App\Http\Controllers\RolController::class, 'updaterol'])->name('updaterol');
    Route::post('rol/delete', [App\Http\Controllers\RolController::class, 'deleterol'])->name('rol/delete');



    

    Route::resource('departamentos', DepartamentoController::class);
    Route::post('dep/add/save', [App\Http\Controllers\DepartamentoController::class, 'addNewDepSave'])->name('dep/add/save');
    Route::post('updatedep', [App\Http\Controllers\DepartamentoController::class, 'updatedep'])->name('updatedep');
    Route::post('dep/delete', [App\Http\Controllers\DepartamentoController::class, 'deletedep'])->name('dep/delete');


    Route::resource('clientes', ClienteController::class);
    Route::post('cliente/add/save', [App\Http\Controllers\ClienteController::class, 'addNewClienteSave'])->name('cliente/add/save');
    Route::post('updatecliente', [App\Http\Controllers\ClienteController::class, 'updatecliente'])->name('updatecliente');
    Route::post('cliente/delete', [App\Http\Controllers\ClienteController::class, 'deletecliente'])->name('cliente/delete');
    Route::get('clientes/perfil/{id}', [App\Http\Controllers\ClienteController::class, 'ver_cliente'])->middleware('auth');


    Route::resource('productos', ProductoController::class);
    Route::post('producto/add/save', [App\Http\Controllers\ProductoController::class, 'addNewProductoSave'])->name('producto/add/save');
    Route::post('updateproducto', [App\Http\Controllers\ProductoController::class, 'updateproducto'])->name('updateproducto');
    Route::post('producto/delete', [App\Http\Controllers\ProductoController::class, 'deleteproducto'])->name('producto/delete');

    Route::resource('transferencias', ChequesController::class);

    Route::post('updatepago', [App\Http\Controllers\ChequesController::class, 'updatepago'])->name('updatepago');

    Route::get('home', function () {
        return view('home');
    });
});

Auth::routes();


function set_active($route)
{
    if (is_array($route)) {
        return in_array(Request::path(), $route) ? 'active' : '';
    }
    return Request::path() == $route ? 'active' : '';
}

// ----------------------------- main dashboard ------------------------------//
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('em/dashboard', [App\Http\Controllers\HomeController::class, 'emDashboard'])->name('em/dashboard');

// -----------------------------settings----------------------------------------//
Route::get('company/settings/page', [App\Http\Controllers\SettingController::class, 'companySettings'])->middleware('auth')->name('company/settings/page');
Route::get('roles/permissions/page', [App\Http\Controllers\SettingController::class, 'rolesPermissions'])->middleware('auth')->name('roles/permissions/page');
Route::post('roles/permissions/save', [App\Http\Controllers\SettingController::class, 'addRecord'])->middleware('auth')->name('roles/permissions/save');
Route::post('roles/permissions/update', [App\Http\Controllers\SettingController::class, 'editRolesPermissions'])->middleware('auth')->name('roles/permissions/update');
Route::post('roles/permissions/delete', [App\Http\Controllers\SettingController::class, 'deleteRolesPermissions'])->middleware('auth')->name('roles/permissions/delete');

// -----------------------------login----------------------------------------//
Route::get('/login', [App\Http\Controllers\Auth\LoginController::class, 'login'])->name('login');
Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'authenticate']);
Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');



// ------------------------------ register ---------------------------------//
Route::get('/register', [App\Http\Controllers\Auth\RegisterController::class, 'register'])->name('register');
Route::post('/register', [App\Http\Controllers\Auth\RegisterController::class, 'storeUser'])->name('register');

// ----------------------------- forget password ----------------------------//
Route::get('forget-password', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'getEmail'])->name('forget-password');
Route::post('forget-password', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'postEmail'])->name('forget-password');

// ----------------------------- reset password -----------------------------//
Route::get('reset-password/{token}', [App\Http\Controllers\Auth\ResetPasswordController::class, 'getPassword']);
Route::post('reset-password', [App\Http\Controllers\Auth\ResetPasswordController::class, 'updatePassword']);

// ----------------------------- user profile ------------------------------//
Route::get('profile_user', [App\Http\Controllers\UserManagementController::class, 'profile'])->middleware('auth')->name('profile_user');
Route::post('profile/information/save', [App\Http\Controllers\UserManagementController::class, 'profileInformation'])->name('profile/information/save');

// ----------------------------- user userManagement -----------------------//
Route::get('usuarios', [App\Http\Controllers\UserManagementController::class, 'index'])->middleware('auth')->name('usuarios');
Route::get('vendedores', [App\Http\Controllers\UserManagementController::class, 'vendedores'])->middleware('auth')->name('vendedores');


Route::post('user/add/save', [App\Http\Controllers\UserManagementController::class, 'addNewUserSave'])->name('user/add/save');
Route::get('search/user/list', [App\Http\Controllers\UserManagementController::class, 'searchUser'])->name('search/user/list');
Route::post('update', [App\Http\Controllers\UserManagementController::class, 'update'])->name('update');
Route::post('user/delete', [App\Http\Controllers\UserManagementController::class, 'delete'])->middleware('auth')->name('user/delete');




Route::get('registro/cambios', [App\Http\Controllers\UserManagementController::class, 'activityLog'])->middleware('auth')->name('registro/cambios');
Route::get('registro/sesiones', [App\Http\Controllers\UserManagementController::class, 'activityLogInLogOut'])->middleware('auth')->name('registro/sesiones');



Route::get('usuarios/perfil/{id}', [App\Http\Controllers\UserManagementController::class, 'ver_usuario'])->middleware('auth');
Route::get('vendedores/perfil/{id}', [App\Http\Controllers\UserManagementController::class, 'ver_vendedor'])->middleware('auth');


// ----------------------------- search user management ------------------------------//
Route::post('search/user/list', [App\Http\Controllers\UserManagementController::class, 'searchUser'])->name('search/user/list');

// ----------------------------- form change password ------------------------------//
Route::get('perfil/contraseña', [App\Http\Controllers\UserManagementController::class, 'changePasswordView'])->middleware('auth')->name('perfil/contraseña');

Route::post('change/password/db', [App\Http\Controllers\UserManagementController::class, 'changePasswordDB'])->name('change/password/db');












