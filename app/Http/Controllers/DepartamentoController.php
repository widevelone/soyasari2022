<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Models\User;
use App\Models\Departamento;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Registro_cambio;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class DepartamentoController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-departamento|crear-departamento|editar-departamento|borrar-departamento', ['only' => ['index']]);
        $this->middleware('permission:crear-departamento', ['only' => ['addNewDepSave']]);
        $this->middleware('permission:editar-departamento', ['only' => ['updatedep']]);
        $this->middleware('permission:borrar-departamento', ['only' => ['deletedep']]);
    }
    public function index()
    {
        $departamentos  = DB::table('departamentos')->get();
        $user = User::all();
        return view('department.departamentos', compact('departamentos', 'user'));
    }
    // save new dep
    public function addNewDepSave(Request $request)
    {
        try {
            $dep = new Departamento;
            $dep->nombre = $request->name;
            $dep->save();


            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Departamento';
            $actividad->registro = $request->name;
            $actividad->accion = 'Creado';
            $actividad->date_time = $todayDate;
            $actividad->save();



            Toastr::success('Departamento Creado', 'Listo');
            return redirect()->route('departamentos.index');
        } catch (\Throwable $th) {
            Toastr::error('Departamento ya existe, revise', 'Listo');
            return redirect()->route('departamentos.index');
        }
    }

    // update
    public function updatedep(Request $request)
    {
        try {

            $department = $request->name;
            $id = $request->id;
            $update = [
                'nombre' => $department,
            ];
            Departamento::where('id', $id)->update($update);

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Departamento';
            $actividad->registro = $request->name;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();

            Toastr::success('Departamento Actualizado', 'Listo');
            return redirect()->route('departamentos.index');
        } catch (\Throwable $th) {
            Toastr::error('Departamento ya existe, revise', 'Listo');
            return redirect()->route('departamentos.index');
        }
    }
    // delete
    public function deletedep(Request $request)
    {
        $dep=Departamento::find($request->id);
        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Departamento';
        $actividad->registro = $dep->nombre;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Departamento::destroy($request->id);
        Toastr::success('Departamento Eliminado', 'Listo');
        return redirect()->route('departamentos.index');
    }
}
