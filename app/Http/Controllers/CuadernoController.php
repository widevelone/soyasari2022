<?php

namespace App\Http\Controllers;

use App\Models\Cuaderno;
use Illuminate\Http\Request;
use App\Models\Historial_cuaderno;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Registro_cambio;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Auth;
use App\Models\Cliente;

class CuadernoController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-cuaderno|crear-cuaderno', ['only' => ['index']]);
        $this->middleware('permission:crear-cuaderno', ['only' => ['addNewCuadernoSave']]);
    }
    public function index()
    {
        $historiales = Historial_cuaderno::all();
        $cuadernos = Cuaderno::all()->where('estado', 'abierto');
        $cuadernosc = Cuaderno::all()->where('estado', 'cerrado');
        $usuarios      =  User::all();
        $clientes    =  Cliente::all();
        return view('cuadernos.cuadernos', compact('usuarios', 'cuadernos', 'cuadernosc', 'clientes', 'historiales'));
    }
    public function addNewCuadernoSave(Request $request)
    {

        $ultimocuaderno = Cuaderno::orderBy('id', 'desc')
            ->first();
        $cuaderno = new Cuaderno();
        $cuaderno->user_id = request()->user_id;
        $cuaderno->codigo = "C-" . ($ultimocuaderno ? $ultimocuaderno->id + 1 : 1);
        $cuaderno->total_venta = 0;
        $cuaderno->total_cobranza = 0;
        $cuaderno->total_gasto = 0;
        $cuaderno->total_garantia = 0;
        $cuaderno->total_deuda = 0;
        $cuaderno->total_pagos_confirmar = 0;
        $cuaderno->total_cuaderno = 0;
        $cuaderno->total_turril_recogido = 0;
        $cuaderno->estado = 'abierto';
        $cuaderno->moneda = 0;
        $cuaderno->billete = 0;
        $cuaderno->dolar = 0;
        $cuaderno->faltante = 0;
        $cuaderno->save();

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Cuadernos';
        $actividad->registro = $cuaderno->codigo;
        $actividad->accion = 'Creado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Toastr::success('Cuaderno Creado', 'Listo');
        return redirect()->route('cuadernos.index');
    }

    public function updatecuaderno(Request $request)
    {
        try {
            $user_id = $request->user_id;
            $update = [
                'user_id' => $user_id,
            ];
            Cuaderno::where('id', $request->id)->update($update);
            Toastr::success('Cuaderno Actualizado', 'Listo');
            return redirect()->route('cuadernos.index');
        } catch (\Throwable $th) {
            Toastr::error('Error', 'Revise');
            return redirect()->route('cuadernos.index');
        }
    }

    public function updateCreated_at(Request $request)
    {
        try {
            $user_id = $request->user_id;
            $update = [
                'user_id' => $user_id,
            ];
            $cuaderno = Cuaderno::where('id', $request->id)
                ->first();

            $oldDate = date('d-m-Y', strtotime($cuaderno->created_at));

            $cuaderno->created_at = $request->created_at;

            $cuaderno->save();

            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Cuadernos';
            $actividad->registro = $cuaderno->codigo;
            $actividad->accion = 'Cambio de fecha de apertura: ' . $oldDate . ' a ' . date('d-m-Y', strtotime($request->created_at));
            $actividad->date_time = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $actividad->save();

            Toastr::success('Cuaderno Actualizado', 'Listo');
            return redirect()->route('cuadernos.index');
        } catch (\Throwable $th) {
            Toastr::error($th->getMessage());
            return redirect()->route('cuadernos.index');
        }
    }
}
