<?php

namespace App\Http\Controllers;

use App\Models\Historial_almacen;
use Illuminate\Http\Request;

class HistorialAlmacenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Historial_almacen  $historial_almacen
     * @return \Illuminate\Http\Response
     */
    public function show(Historial_almacen $historial_almacen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Historial_almacen  $historial_almacen
     * @return \Illuminate\Http\Response
     */
    public function edit(Historial_almacen $historial_almacen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Historial_almacen  $historial_almacen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historial_almacen $historial_almacen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Historial_almacen  $historial_almacen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Historial_almacen $historial_almacen)
    {
        //
    }
}
