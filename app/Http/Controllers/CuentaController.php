<?php

namespace App\Http\Controllers;

use App\Models\Cuenta;
use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Cuenta_turrile;
use App\Models\Gastos_cuaderno_diario;
use App\Models\Historial_cuaderno;
use App\Models\Producto;
use App\Models\Registro_cambio;
use Illuminate\Http\Request;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CuentaController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:ver-deuda', ['only' => ['index']]);
        $this->middleware('permission:ver-prestamos-y-garantias', ['only' => ['prestamosgarantias']]);
        $this->middleware('permission:ver-gasto', ['only' => ['gastos']]);
        $this->middleware('permission:ver-incentivo', ['only' => ['incentivos']]);
    }

    public function index()
    {
        $cuentas_pen      =   Cuenta::all()->where('estado', 'pendiente');
        $cuentas_pag   =   Cuenta::all()->where('estado', 'completado');
        return view('cuentas.cuentas', compact('cuentas_pen', 'cuentas_pag'));
    }

    public function ver_cuenta($id)
    {
        $cuenta = Cuenta::find($id);
        return view('cuentas.detalle', compact('cuenta'));
    }
    public function prestamos()
    {
        $cuenta_turriles = Cuenta_turrile::all();
        return view('cuentas.prestamos', compact('cuenta_turriles'));
    }
    public function garantias()
    {
        $cuenta_turriles = Cuenta_turrile::all();
        return view('cuentas.garantias', compact('cuenta_turriles'));
    }



    public function registraranteriores()
    {
        $productos = Producto::all();
        $clientes = Cliente::all();
        $vendedores = User::all()->where('departamento_id', '2');
        return view('cuentas.anterior', compact('productos', 'clientes', 'vendedores'));
    }
    public function gastos()
    {
        $gastos = Gastos_cuaderno_diario::all();
        $vendedores = User::all()->where('departamento_id', '2');
        return view('cheques.gastos', compact('gastos', 'vendedores'));
    }

    public function incentivos()
    {

        $vendedores = User::all()->where('departamento_id', '2');
        $productos = Producto::all();
        return view('cheques.incentivos', compact('vendedores', 'productos'));
    }

    public function addNewAnteriorSave(Request $request)
    {

        $historial_cuaderno = new Historial_cuaderno();
        $historial_cuaderno->producto_id = $request->producto_id;
        $historial_cuaderno->cliente_id = $request->cliente_id;
        $historial_cuaderno->precio_unitario = $request->precio_unitario;
        $historial_cuaderno->cantidad = $request->cantidad;
        $historial_cuaderno->total_venta = $request->total_venta;
        $historial_cuaderno->total_cobrado = $request->total_cobrado;
        $historial_cuaderno->total_deuda = $request->total_saldo;
        $historial_cuaderno->user_id = $request->user_id;
        $historial_cuaderno->save();
        $ultimo_historial = Historial_cuaderno::orderBy('id', 'desc')->first();

        $cuenta = new Cuenta();
        $cuenta->fecha_limite = $request->fecha_limite;
        $cuenta->saldo_inicial = $request->total_saldo;
        $cuenta->saldo = $request->total_saldo;
        $cuenta->estado = 'pendiente';
        $cuenta->historial_cuaderno_id = $ultimo_historial->id;
        $cuenta->save();


        $productos = Producto::all();
        $clientes = Cliente::all();
        $vendedores = User::all()->where('departamento_id', '2');
        return view('cuentas.anterior', compact('productos', 'clientes', 'vendedores'));
    }

    public function updateFechaLimite(Request $request)
    {
        try {

            $cuenta = Cuenta::find($request->id);

            $oldDate = $cuenta->fecha_limite;

            $cuenta->fecha_limite = $request->fecha_limite;
            $cuenta->save();

            $historial_cuaderno = Historial_cuaderno::find($cuenta->historial_cuaderno_id);
            $cliente = Cliente::find($historial_cuaderno->cliente_id);

            $todayDate = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Deudas';
            $actividad->registro = 'Cliente: ' . $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;
            $actividad->accion = 'Cambio de fecha límite: ' . $oldDate . ' a ' . date('d-m-Y', strtotime($request->fecha_limite));
            $actividad->date_time = $todayDate;
            $actividad->save();

            Toastr::success('Fecha límite Actualizada', 'Listo');
            return redirect()->route('cuentas.index');
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect()->route('cuentas.index');
        }
    }
    
    public function updateFechaLimitePrestamoTurriles(Request $request)
    {
        try {

            $cuenta = Cuenta_turrile::find($request->id);

            $oldDate = $cuenta->fecha_limite;

            $cuenta->fecha_limite = $request->fecha_limite;
            $cuenta->save();

            $historial_cuaderno = Historial_cuaderno::find($cuenta->historial_cuaderno_id);
            $cliente = Cliente::find($historial_cuaderno->cliente_id);

            $todayDate = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Prestamos';
            $actividad->registro = 'Cliente: ' . $cliente->nombre . ' ' . $cliente->apellido_paterno . ' ' . $cliente->apellido_materno;
            $actividad->accion = 'Cambio de fecha límite: ' . $oldDate . ' a ' . date('d-m-Y', strtotime($request->fecha_limite));
            $actividad->date_time = $todayDate;
            $actividad->save();

            Toastr::success('Fecha límite Actualizada', 'Listo');
            return redirect()->route('prestamos');
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect()->route('prestamos');
        }
    }




    public function searchUser(Request $request)
    {


        if ($request->fecha) {
            $result = User::where('name', 'LIKE', '%' . $request->name . '%')->get();
        }

        // search by role name
        if ($request->role_name) {
            $result = User::where('role_name', 'LIKE', '%' . $request->role_name . '%')->get();
        }

        // search by status
        if ($request->status) {
            $result = User::where('status', 'LIKE', '%' . $request->status . '%')->get();
        }

        // search by name and role name
        if ($request->name && $request->role_name) {
            $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                ->where('role_name', 'LIKE', '%' . $request->role_name . '%')
                ->get();
        }

        // search by role name and status
        if ($request->role_name && $request->status) {
            $result = User::where('role_name', 'LIKE', '%' . $request->role_name . '%')
                ->where('status', 'LIKE', '%' . $request->status . '%')
                ->get();
        }

        // search by name and status
        if ($request->name && $request->status) {
            $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                ->where('status', 'LIKE', '%' . $request->status . '%')
                ->get();
        }

        // search by name and role name and status
        if ($request->name && $request->role_name && $request->status) {
            $result = User::where('name', 'LIKE', '%' . $request->name . '%')
                ->where('role_name', 'LIKE', '%' . $request->role_name . '%')
                ->where('status', 'LIKE', '%' . $request->status . '%')
                ->get();
        }

        $vendedores = User::all()->where('departamento_id', '2');
        Toastr::success($request->fecha, 'Listo');
        return view('cheques.incentivos', compact('vendedores'));
    }
}
