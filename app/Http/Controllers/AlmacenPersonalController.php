<?php

namespace App\Http\Controllers;

use App\Models\Almacen_personal;
use App\Models\His_almacen_personal;
use App\Models\Producto;
use App\Models\Registro_cambio;
use App\Models\Transportista;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;

class AlmacenPersonalController extends Controller
{
    public function index()
    {
        // $almacen_personales = Producto::all();
        $productos = DB::table('productos')
            ->get();

        $almacen_personales = Almacen_personal::all();

        foreach ($almacen_personales as $item) {
            $cli = DB::table('clientes')
                ->where('codigo', '=', $item->user->email)
                ->first();

            if ($cli != null) {
                $item->user->tipo_negocio = $cli->tipo_negocio;
                $item->user->avatar = $cli->avatar;
            }
        }

        $clientes = DB::table('clientes')
            ->join('users', 'users.email', '=', 'clientes.codigo')
            ->select(
                'users.id as user_id',
                'users.email as codigo',
                'clientes.nombre',
                'clientes.apellido_paterno',
                'clientes.apellido_materno',
                'clientes.tipo_negocio',
                'clientes.avatar',
            )
            ->where('tipo_negocio', '=', 'Empresa')
            ->orWhere('tipo_negocio', '=', 'Mayorista')
            ->orWhere('tipo_negocio', '=', 'Fábrica')
            ->orWhere('tipo_negocio', '=', 'Fabrica')
            ->get();

        return view('almacens.almacenpersonales', compact('productos', 'almacen_personales', 'clientes'));
    }

    public function addNewAlmacenPersonalSave(Request $request)
    {
        $almacen = new Almacen_personal();
        $almacen->producto_id = $request->producto_id;
        $almacen->user_id = $request->user_id;
        $almacen->estado = $request->estado;
        $almacen->cantidad_anterior = 0;
        $almacen->cantidad_actual = $request->cantidad_actual;
        $almacen->save();

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes personales';
        $actividad->registro = $almacen->producto->nombre . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Creado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Toastr::success('Stok Creado', 'Listo');
        return redirect()->route('almacenes_personales.index');
    }

    public function delete(Request $request)
    {

        $almacen = Almacen_personal::find($request->id);

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes personal';
        $actividad->registro = $almacen->producto->nombre . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Almacen_personal::destroy(($request->id));
        Toastr::success('Almacen personal Eliminado', 'Listo');
        return redirect()->route('almacenes_personales.index');
    }

    public function registrarAlmacenPersonal($id)
    {

        $historial_almacen_personales = His_almacen_personal::orderBy('id', 'desc')->where('almacen_personal_id', $id)->get();
        $almacen_personal = Almacen_personal::all()->where('id', $id)->first();
        $cli = DB::table('clientes')
                ->where('codigo', '=', $almacen_personal->user->email)
                ->first();

            if ($cli != null) {
                $almacen_personal->user->tipo_negocio = $cli->tipo_negocio;
                $almacen_personal->user->avatar = $cli->avatar;
            }
        $usuarios = User::all();
        $transportistas = Transportista::all();
        return view('almacens.historialpersonal', compact('historial_almacen_personales', 'almacen_personal', 'usuarios', 'transportistas'));
    }



    // public function registrarAlmacen($id)
    // {

    //     $historial_almacenes = Historial_almacen::orderBy('id', 'desc')->where('almacen_id', $id)->get();
    //     $almacen = Almacen::all()->where('id', $id)->first();
    //     $usuarios = User::all();
    //     $transportistas = Transportista::all();
    //     return view('almacens.historial', compact('historial_almacenes', 'almacen', 'usuarios', 'transportistas'));
    // }


    public function addNewHistorialalmacenPersonalSave(Request $request)
    {
        $almacen_personal_id = $request->almacen_personal_id;
        $historial_almacen = new His_almacen_personal();
        $historial_almacen->almacen_personal_id = $almacen_personal_id;
        $historial_almacen->movimiento = $request->movimiento;
        $historial_almacen->user_id = $request->user_id;
        $historial_almacen->codigo = $request->codigo;
        $historial_almacen->cantidad = $request->cantidad;
        $historial_almacen->obs = $request->obs;

        $almacen = Almacen_personal::find($almacen_personal_id);
        if ($request->movimiento == 'Salida Ventas' || $request->movimiento == 'Salida de Envase') {
            $historial_almacen->saldo = $almacen->cantidad_actual - $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual - $request->cantidad;
        } else {
            $historial_almacen->saldo = $almacen->cantidad_actual + $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual + $request->cantidad;
        }
        $almacen->save();
        $historial_almacen->save();


        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes personales';
        $actividad->registro = $request->movimiento . ', ' . $request->cantidad . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Actualizado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Toastr::success('Registro Creado', 'Listo');
        return redirect('almacenes_personales/registrar/' . $almacen_personal_id);
    }

    public function addNewHistorialalmacenPersonalTransportistaSave(Request $request)
    {
        $almacen_personal_id = $request->almacen_personal_id;
        $historial_almacen = new His_almacen_personal();
        $historial_almacen->almacen_personal_id = $almacen_personal_id;
        $historial_almacen->movimiento = $request->movimiento;
        $historial_almacen->transportista_id = $request->transportista_id;
        $historial_almacen->codigo = $request->codigo;
        $historial_almacen->cantidad = $request->cantidad;
        $historial_almacen->obs = $request->obs;

        $almacen = Almacen_personal::find($almacen_personal_id);
        if ($request->movimiento == 'Salida Ventas' || $request->movimiento == 'Salida de Envase') {
            $historial_almacen->saldo = $almacen->cantidad_actual - $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual - $request->cantidad;
        } else {
            $historial_almacen->saldo = $almacen->cantidad_actual + $request->cantidad;
            $almacen->cantidad_anterior = $almacen->cantidad_actual;
            $almacen->cantidad_actual = $almacen->cantidad_actual + $request->cantidad;
        }
        $almacen->save();
        $historial_almacen->save();


        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes personales';
        $actividad->registro = $request->movimiento . ', ' . $request->cantidad . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Actualizado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        Toastr::success('Registro Creado', 'Listo');
        return redirect('almacenes_personales/registrar/' . $almacen_personal_id);
    }

    public function deletehis(Request $request)
    {
        $almacen = Almacen_personal::find($request->almacen_personal_id);
        $historial_almacen = His_almacen_personal::find($request->id);

        if ($historial_almacen->movimiento == 'Salida Ventas' || $historial_almacen->movimiento == 'Salida de Envase') {
            $almacen->cantidad_actual = $almacen->cantidad_actual + $historial_almacen->cantidad;
        } else {
            $almacen->cantidad_actual = $almacen->cantidad_actual - $historial_almacen->cantidad;
        }
        $almacen->save();

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Almacenes';
        $actividad->registro = $historial_almacen->movimiento . ', ' . $historial_almacen->cantidad . ' ' . $almacen->producto->descripcion . ' ' . $almacen->producto->capacidad . ', ' . $almacen->estado;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();


        His_almacen_personal::destroy(($request->id));
        Toastr::success('Movimiento Eliminado', 'Listo');
        return redirect('almacenes_personales/registrar/' . $almacen->id);
    }
}
