<?php

namespace App\Http\Controllers;

use App\Models\Historial_cuaderno;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cliente;
use App\Models\Cuaderno;
use App\Models\Cuenta;
use App\Models\Gastos_cuaderno_diario;
use App\Models\Historial_cuenta;
use App\Models\Producto;
use App\Models\Cuenta_turrile;
use App\Models\Historial_cuadeno_almacen;
use App\Models\Historial_cuadeno_almacenes;
use App\Models\Historial_cuenta_turrile;
use App\Models\Registro_cambio;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class HistorialCuadernoController extends Controller
{

    function __construct()
    {
        
        $this->middleware('permission:editar-cuaderno', ['only' => ['registrarCuaderno']]);
        $this->middleware('permission:borrar-registros-cuaderno', ['only' => ['deletehis','deletegasto']]);
        
    }

    public function addNewHistorialSave(Request $request)
    {
        $historial = new Historial_cuaderno();
        $historial->cuaderno_id = request()->cuaderno_id;
        $historial->cliente_id = request()->cliente_id;
        $historial->producto_id = request()->producto_id;

        $historial->cantidad = request()->cantidad;
        $historial->precio_unitario = request()->precio_unitario;
        $historial->metodo_pago = request()->metodo_pago;
        $historial->cantidad_vacio = request()->cantidad_vacio;
        $historial->metodo_vacio = request()->metodo_vacio;
        $historial->estado_turril = request()->estado_turril;

        $historial->total_venta = request()->total_venta;
        $historial->total_cobrado = request()->total_cobrado;

        $historial->total_deuda = request()->total_venta - request()->total_cobrado;

        $historial->obs = request()->obs;

        $cua_id = request()->cuaderno_id;
        $cuaderno = Cuaderno::find($cua_id);
        $cuaderno->total_venta = $cuaderno->total_venta + $historial->total_venta;
        $cuaderno->total_deuda = $cuaderno->total_deuda + $historial->total_deuda;

        if (request()->metodo_pago != 'Efectivo') {
            $historial->metodo_pago_estado = 'pendiente';
            $cuaderno->total_pagos_confirmar = $cuaderno->total_pagos_confirmar + $historial->total_cobrado;
        } else {
            $historial->metodo_pago_estado = 'normal';
            $cuaderno->total_cuaderno = $cuaderno->total_cuaderno + $historial->total_cobrado;
        }



        $historial->save();

        if ($historial->total_deuda > 0) {
            $ultimo_historial = Historial_cuaderno::orderBy('id', 'desc')->first();
            $cuenta = new Cuenta();
            $cuenta->historial_cuaderno_id = $ultimo_historial->id;
            $cuenta->fecha_limite = $request->fecha_limite_cuenta;
            $cuenta->saldo_inicial = $historial->total_deuda;
            $cuenta->saldo = $historial->total_deuda;
            $cuenta->estado = 'pendiente';
            $cuenta->save();
        }

        if ($request->metodo_vacio == 'garantia') {
            $ultimo_historial = Historial_cuaderno::orderBy('id', 'desc')->first();
            $cuenta_garantia = new Cuenta_turrile();
            $cuenta_garantia->saldo_cantidad_inicial = $request->cantidad - $request->cantidad_vacio;
            $cuenta_garantia->saldo_cantidad = $request->cantidad - $request->cantidad_vacio;
            $cuenta_garantia->saldo_garantia = $request->monto_garantia;
            $cuenta_garantia->saldo_garantia_inicial = $request->monto_garantia;
            $cuenta_garantia->estado = 'pendiente';
            $cuenta_garantia->historial_cuaderno_id = $ultimo_historial->id;
            $cuaderno->total_garantia = $cuaderno->total_garantia + $request->monto_garantia;
            $cuaderno->total_cuaderno = $cuaderno->total_cuaderno + $request->monto_garantia;
            $cuenta_garantia->save();
        }

        if ($request->metodo_vacio == 'prestamo') {
            $ultimo_historial = Historial_cuaderno::orderBy('id', 'desc')->first();
            $cuenta_garantia = new Cuenta_turrile();
            $cuenta_garantia->saldo_cantidad_inicial = $request->cantidad - $request->cantidad_vacio;
            $cuenta_garantia->saldo_cantidad = $request->cantidad - $request->cantidad_vacio;
            $cuenta_garantia->fecha_limite = $request->fecha_limite_turril;
            $cuenta_garantia->estado = 'pendiente';
            $cuenta_garantia->historial_cuaderno_id = $ultimo_historial->id;
            $cuenta_garantia->save();
        }

        $cuaderno->save();

        Toastr::success('Registro Creado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }

    public function addNewGastoSave(Request $request)
    {
        $gasto = new Gastos_cuaderno_diario();
        $gasto->descripcion = request()->descripcion;
        $gasto->monto = request()->monto_gasto;
        $gasto->cuaderno_id = request()->cuaderno_id;
        $gasto->save();

        $cua_id = request()->cuaderno_id;
        $cuaderno = Cuaderno::find($cua_id);
        $cuaderno->total_gasto = $cuaderno->total_gasto + request()->monto_gasto;
        $cuaderno->total_cuaderno = $cuaderno->total_cuaderno - request()->monto_gasto;
        $cuaderno->save();

        Toastr::success('Registro Creado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }


    public function addNewEfectivoSave(Request $request)
    {
        
        $cua_id = request()->cuaderno_id;
        $cuaderno = Cuaderno::find($cua_id);
        $cuaderno->moneda=$request->moneda;
        $cuaderno->billete=$request->billete;
        $cuaderno->dolar=$request->dolar;
        $cuaderno->faltante=$request->faltante;
        $cuaderno->save();

        Toastr::success('Registro Creado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }


    public function addNewTurrilSave(Request $request)
    {

        $cuenta_turril = Cuenta_turrile::find($request->cuenta_turril_id);

        $cua_id = request()->cuaderno_id;

        if ($cuenta_turril->historial_cuaderno->metodo_vacio == 'garantia') {
            $gasto = new Gastos_cuaderno_diario();
            $gasto->descripcion = 'Garantia por ' . $request->cantidad_turriles . ' Turriles de ' . $cuenta_turril->historial_cuaderno->cliente->nombre . ' ' . $cuenta_turril->historial_cuaderno->cliente->apellido_paterno;
            $gasto->monto = $cuenta_turril->saldo_garantia;
            $gasto->cuaderno_id = request()->cuaderno_id;
            $gasto->save();

            $cuaderno = Cuaderno::find($cua_id);
            $cuaderno->total_gasto = $cuaderno->total_gasto + $gasto->monto;
            $cuaderno->total_cuaderno = $cuaderno->total_cuaderno - $gasto->monto;
            $cuaderno->save();

            $historial_cuenta_turril = new Historial_cuenta_turrile();
            $historial_cuenta_turril->cantidad_turril = $request->cantidad_turriles;
            $historial_cuenta_turril->cuenta_turril_id = $cuenta_turril->id;
            $historial_cuenta_turril->cuaderno_id = $cua_id;
            $historial_cuenta_turril->save();

            $cuenta_turril->saldo_garantia = '0';
            $cuenta_turril->saldo_cantidad = $cuenta_turril->saldo_cantidad - $request->cantidad_turriles;
            if ($cuenta_turril->saldo_garantia == '0' && $cuenta_turril->saldo_cantidad == '0') {
                $cuenta_turril->estado = 'entregado';
            }
        }

        if ($cuenta_turril->historial_cuaderno->metodo_vacio == 'prestamo') {

            $historial_cuenta_turril = new Historial_cuenta_turrile();
            $historial_cuenta_turril->cantidad_turril = $request->cantidad_turriles;
            $historial_cuenta_turril->cuenta_turril_id = $cuenta_turril->id;
            $historial_cuenta_turril->cuaderno_id = $cua_id;
            $historial_cuenta_turril->save();
            $cuenta_turril->saldo_cantidad = $cuenta_turril->saldo_cantidad - $request->cantidad_turriles;
            if ($cuenta_turril->saldo_cantidad == '0') {
                $cuenta_turril->estado = 'entregado';
            }
        }
        $cuenta_turril->save();

        Toastr::success('Registro Creado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }


    public function registrarCuaderno($id)
    {
        $cuaderno = DB::table('cuadernos')->where('id', $id)->first();
        $user_id = $cuaderno->user_id;
        $usuario = DB::table('users')->where('id', $user_id)->first();
        $productos = Producto::all();
        $historiales = Historial_cuaderno::orderBy('id', 'desc')->where('cuaderno_id', $id)->get();
        $historial_cuentas = Historial_cuenta::orderBy('id', 'desc')->where('cuaderno_id', $id)->get();
        $clientes = Cliente::all()->where('user_id', $user_id);
        $cuentas = Cuenta::all();
        $gastos = Gastos_cuaderno_diario::orderBy('id', 'desc')->where('cuaderno_id', $id)->get();
        $cuentas_turril = Cuenta_turrile::all();
        $historial_cuenta_turrile = Historial_cuenta_turrile::orderBy('id', 'desc')->where('cuaderno_id', $id)->get();

        $historial_cuaderno_almacenes = Historial_cuadeno_almacen::all()->where('created_ad', $cuaderno->created_at)->where('user_id', $usuario->id);
        return view('cuadernos.historial', compact('historial_cuaderno_almacenes', 'historial_cuenta_turrile', 'cuentas_turril', 'cuaderno', 'usuario', 'productos', 'clientes', 'historiales', 'cuentas', 'historial_cuentas', 'gastos'));
    }

    public function deletehis(Request $request)
    {

        $cua_id = $request->cuaderno_id;
        $historial = Historial_cuaderno::find($request->id);
        
        $cuaderno = Cuaderno::find($cua_id);
        if($historial->metodo_vacio=='garantia'){
            $cuaderno->total_garantia=$cuaderno->total_garantia-$historial->cuenta_turril[0]->saldo_garantia_inicial;
            $cuaderno->total_cuaderno=$cuaderno->total_cuaderno-$historial->cuenta_turril[0]->saldo_garantia_inicial;
        }
        $cuaderno->total_venta = $cuaderno->total_venta - $historial->total_venta;
        $cuaderno->total_deuda = $cuaderno->total_deuda - $historial->total_deuda;
        if ($historial->metodo_pago != 'Efectivo')
            $cuaderno->total_pagos_confirmar = $cuaderno->total_pagos_confirmar - $historial->total_cobrado;
        else
            $cuaderno->total_cuaderno = $cuaderno->total_cuaderno - $historial->total_cobrado;
        $cuaderno->save();

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Ventas';
        $actividad->registro=$cuaderno->codigo.', '.$historial->cliente->nombre.' '.$historial->cliente->apellido_paterno.', '.$historial->total_cobrado;
        $actividad->accion='Eliminado';
        $actividad->date_time=$todayDate;
        $actividad->save();


        DB::table('cuentas')->where('historial_cuaderno_id', $historial->id)->delete();
        Historial_cuaderno::destroy($request->id);
        Toastr::success('Venta Eliminada', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }

    public function deletegasto(Request $request)
    {
        $cua_id = $request->cuaderno_id;
        $gasto = Gastos_cuaderno_diario::find($request->id);
        $cuaderno = Cuaderno::find($cua_id);
        $cuaderno->total_gasto = $cuaderno->total_gasto - $gasto->monto;
        $cuaderno->total_cuaderno = $cuaderno->total_cuaderno + $gasto->monto;
        $cuaderno->save();

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad= new Registro_cambio();
        $actividad->user_name= Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo= Auth::user()->email;
        $actividad->tabla= 'Gastos';
        $actividad->registro=$cuaderno->codigo.', '.$gasto->descripcion.' '.$gasto->monto;
        $actividad->accion='Eliminado';
        $actividad->date_time=$todayDate;
        $actividad->save();
        Gastos_cuaderno_diario::destroy($request->id);
        Toastr::success('Gasto Eliminado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }

    public function cerrar(Request $request)
    {
        $cua_id = $request->cuaderno_id;
        $cuaderno = Cuaderno::find($cua_id);
        $cuaderno->estado='cerrado';
        $cuaderno->save();
        Toastr::success('Cuaderno Cerrado', 'Listo');
        return redirect('cuadernos/registrar/' . $cua_id);
    }
}
