<?php

namespace App\Http\Controllers;

use App\Models\Descuento_personal_venta;
use App\Models\His_descuento_personal_venta;
use App\Models\Registro_cambio;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DescuentoPVController extends Controller
{
    public function index()
    {
        $descuento_personal_ventas_abierto = Descuento_personal_venta::where('estado', '=', 'abierto')
            ->get();
        $descuento_personal_ventas_cerrado = Descuento_personal_venta::where('estado', '=', 'cerrado')
            ->get();
        $usuarios = User::where('departamento_id', '=', 2)
            ->get();
        return view('tesoreria.descuentos', compact('usuarios', 'descuento_personal_ventas_abierto', 'descuento_personal_ventas_cerrado'));
    }

    public function addNew(Request $request)
    {
        $descuento = new Descuento_personal_venta();
        $descuento->total = 0;
        $descuento->user_id = $request->user_id;
        $descuento->estado = 'abierto';
        $descuento->save();

        $userD = User::find($request->user_id);

        $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Descuento personal venta';
        $actividad->registro = $userD->nombre . ' ' . $userD->apellido_paterno . '' . $userD->apellido_materno;
        $actividad->accion = 'Creado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Toastr::success('Stock Creado', 'Listo');
        return redirect()->route('descuentos.index');
    }

    public function removeDescuento(Request $request)
    {
        try {

            $hisList = His_descuento_personal_venta::where('descuento_personal_venta_id', '=', $request->id)->get();
            echo ($request);
            foreach ($hisList as $item) {
                His_descuento_personal_venta::destroy($item->id);
            }
            $desc = Descuento_personal_venta::find($request->id);

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Historial descuentos';
            $actividad->registro =  $desc->user->nombre . ' ' . $desc->user->apellido_paterno . '' . $desc->user->apellido_materno;
            $actividad->accion = 'Eliminado';
            $actividad->date_time = $todayDate;
            $actividad->save();

            Descuento_personal_venta::destroy($request->id);

            Toastr::success('Registro Eliminado', 'Listo');
            return redirect()->route('descuentos.index');
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect()->route('descuentos.index');
        }
    }

    public function historialDescuentos($id)
    {

        $historial_descuentos = His_descuento_personal_venta::orderBy('id', 'desc')->where('descuento_personal_venta_id', $id)->get();
        $descuento = Descuento_personal_venta::where('id', $id)->first();

        return view('tesoreria.historialdescuento', compact('historial_descuentos', 'descuento'));
    }

    public function addNewHistorialDescuentos(Request $request)
    {
        try {
            $desc = Descuento_personal_venta::find($request->descuento_personal_venta_id);

            $his = new His_descuento_personal_venta();
            $his->descuento_personal_venta_id = $desc->id;
            $his->fecha = $request->fecha;
            $his->detalle = $request->detalle;
            $his->monto = $request->monto;
            $his->obs = $request->obs;

            // echo $desc;
            $desc->total = $desc->total + $request->monto;

            $his->save();
            $desc->save();

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Historial descuentos';
            $actividad->registro =  $desc->user->nombre . ' ' . $desc->user->apellido_paterno . '' . $desc->user->apellido_materno;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();


            Toastr::success('Registro Creado', 'Listo');
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        }
    }

    public function removeNewHistorialDescuentos(Request $request)
    {
        try {
            $his = His_descuento_personal_venta::find($request->id);

            $desc = Descuento_personal_venta::find($his->descuento_personal_venta_id);
            $desc->total = $desc->total - $his->monto;
            $desc->save();

            His_descuento_personal_venta::destroy($request->id);

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Historial descuentos';
            $actividad->registro =  $desc->user->nombre . ' ' . $desc->user->apellido_paterno . '' . $desc->user->apellido_materno;
            $actividad->accion = 'Eliminado';
            $actividad->date_time = $todayDate;
            $actividad->save();


            Toastr::success('Registro Eliminado', 'Listo');
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        }
    }

    public function descuentoCerrar(Request $request)
    {
        try {

            $desc = Descuento_personal_venta::find($request->id);
            $desc->estado = 'cerrado';

            $desc->save();

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Descuento';
            $actividad->registro =  $desc->user->nombre . ' ' . $desc->user->apellido_paterno . '' . $desc->user->apellido_materno;
            $actividad->accion = 'Descuento cerrado';
            $actividad->date_time = $todayDate;
            $actividad->save();


            Toastr::success('Registro Actualizado', 'Listo');
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/historialdescuentos/' . $desc->id);
        }
    }
}
