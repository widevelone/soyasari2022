<?php

namespace App\Http\Controllers;

use App\Models\Pago_ref_pas_carro;
use App\Models\His_pago_pasaje_refri_carros;
use App\Models\his_pago_pasaje_refri_carros as ModelsHis_pago_pasaje_refri_carros;
use App\Models\Registro_cambio;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PagosRPCVController extends Controller
{

    public function index()
    {
        $pago_ref_pas_carros_abierto = Pago_ref_pas_carro::where('estado', '=', 'abierto')
            ->get();
        $pago_ref_pas_carros_cerrado = Pago_ref_pas_carro::where('estado', '=', 'cerrado')
            ->get();
        return view('tesoreria.pago_ref_pas_carros', compact('pago_ref_pas_carros_abierto', 'pago_ref_pas_carros_cerrado'));
    }
    public function addNew(Request $request)
    {
        $pago = new Pago_ref_pas_carro();
        $pago->total = 0;
        $pago->fecha = $request->fecha;
        $pago->estado = 'abierto';
        $pago->save();

        // $userD = User::find($request->user_id);

        // $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        // $todayDate = $dt;
        // $actividad = new Registro_cambio();
        // $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        // $actividad->codigo = Auth::user()->email;
        // $actividad->tabla = 'Descuento personal venta';
        // $actividad->registro = $userD->nombre . ' ' . $userD->apellido_paterno . '' . $userD->apellido_materno;
        // $actividad->accion = 'Creado';
        // $actividad->date_time = $todayDate;
        // $actividad->save();

        Toastr::success('Stock Creado', 'Listo');
        return redirect()->route('pago_ref_pas_carros.index');
    }

    public function historialPagorpc($id)
    {

        $his_pago_ref_pas_carros = His_pago_pasaje_refri_carros::orderBy('id', 'desc')->where('pago_pasaje_refri_carros_id', $id)->get();
        $pago = Pago_ref_pas_carro::where('id', $id)->first();
        $usuarios = User::where('departamento_id', '=', 2)
            ->get();

        return view('tesoreria.his_pago_ref_pas_carros', compact('his_pago_ref_pas_carros', 'pago', 'usuarios'));
    }

    public function addNewPagorpc(Request $request)
    {
        try {
            $desc = Pago_ref_pas_carro::find($request->pago_pasaje_refri_carros_id);

            $his = new ModelsHis_pago_pasaje_refri_carros();
            $his->pago_pasaje_refri_carros_id = $desc->id;
            $his->user_id = $request->user_id;
            $his->nro_dias = $request->nro_dias;
            $his->monto = $request->monto;
            $his->obs = $request->obs;

            // echo $desc;
            $desc->total = $desc->total + $request->monto;

            $his->save();
            $desc->save();

            // $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            // $todayDate = $dt;
            // $actividad = new Registro_cambio();
            // $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            // $actividad->codigo = Auth::user()->email;
            // $actividad->tabla = 'Historial descuentos';
            // $actividad->registro =  $desc->user->nombre . ' ' . $desc->user->apellido_paterno . '' . $desc->user->apellido_materno;
            // $actividad->accion = 'Actualizado';
            // $actividad->date_time = $todayDate;
            // $actividad->save();


            Toastr::success('Registro Creado', 'Listo');
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $request->pago_pasaje_refri_carros_id);
        }
    }

    public function removeHistorialPagosPcv(Request $request)
    {
        try {
            // echo ($request);
            $his = His_pago_pasaje_refri_carros::find($request->id);

            $desc = Pago_ref_pas_carro::find($his->pago_pasaje_refri_carros_id);
            $desc->total = $desc->total - $his->monto;
            $desc->save();
            
            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Historial pago pasage - refrigerio';
            $actividad->registro =  $his->user->nombre . ' ' . $his->user->apellido_paterno . '' . $his->user->apellido_materno;
            $actividad->accion = 'Eliminado';
            $actividad->date_time = $todayDate;
            $actividad->save();

            His_pago_pasaje_refri_carros::destroy($request->id);
            
            Toastr::success('Registro Eliminado', 'Listo');
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $request->pago_id);
        }
    }

    public function pagoPcvCerrar(Request $request)
    {
        try {

            $desc = Pago_ref_pas_carro::find($request->idv);
            $desc->estado = 'cerrado';

            $desc->save();

            $dt = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Pago pasage - refrigerio';
            $actividad->registro =  $desc->fecha;
            $actividad->accion = 'cerrado';
            $actividad->date_time = $todayDate;
            $actividad->save();


            Toastr::success('Registro Actualizado', 'Listo');
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $desc->id);
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect('tesoreria/his_pago_ref_pas_carros/' . $request->id);
        }
    }
}
