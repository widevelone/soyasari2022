<?php

namespace App\Http\Controllers;

use Brian2694\Toastr\Facades\Toastr;
use App\Models\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Registro_cambio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ClienteController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:ver-cliente|crear-cliente|editar-cliente|borrar-cliente|ver-perfil-cliente', ['only' => ['index']]);
        $this->middleware('permission:crear-cliente', ['only' => ['addNewClienteSave']]);
        $this->middleware('permission:editar-cliente', ['only' => ['updatecliente']]);
        $this->middleware('permission:borrar-cliente', ['only' => ['deletecliente']]);
        $this->middleware('permission:ver-perfil-cliente', ['only' => ['ver_cliente']]);
    }
    public function index()
    {
        $clientes      =  Cliente::all();
        $users      =  User::all();
        return view('clientes.clientes', compact('clientes', 'users'));
    }
    public function ver_cliente($id)
    {
        $cliente = Cliente::find($id);
        return view('clientes.perfil', compact('cliente'));
    }

    // save new cliente
    public function addNewClienteSave(Request $request)
    {
        try {
            $foto = null;
            if ($request->foto_negocio != '') {
                $foto = time() . '.' . $request->foto_negocio->extension();
                $request->foto_negocio->move(public_path('assets/images/clientes'), $foto);
            }

            $cliente = new Cliente;
            $cliente->user_id = request()->user_id;
            $cliente->nombre = request()->nombre;
            $cliente->apellido_paterno = request()->apellido_paterno;
            $cliente->apellido_materno = request()->apellido_materno;
            $cliente->carnet = request()->carnet;
            $cliente->telefono = request()->telefono;
            $cliente->estado = request()->estado;
            $cliente->zona = request()->direccion;
            $cliente->tiempo_consumo_turril = request()->tiempo_consumo_turril;
            $cliente->tipo_negocio = request()->tipo_negocio;
            $cliente->foto_negocio = $foto;
            $cliente->geolocalizacion = request()->geolocalizacion;
            $cliente->obs = request()->obs;
            $cliente->avatar = $cliente->tipo_negocio . '.png';
            $cliente->save();

            $userCodeParam = str_pad(request()->user_id, 3, "0", STR_PAD_LEFT);
            $clientCodeParam = str_pad($cliente->id, 3, "0", STR_PAD_LEFT);

            $code = $userCodeParam . '-' . $clientCodeParam;
            $cliente->codigo = $code;

            $cliente->save();

            $user = new User;

            $user->nombre = request()->nombre;
            $user->apellido_paterno = request()->apellido_paterno;
            $user->apellido_materno = request()->apellido_materno;
            $user->carnet = request()->carnet;
            $user->email = $code;
            $user->telefono = request()->telefono;
            $user->estado = request()->estado;
            $user->direccion = request()->direccion;
            $cliente->avatar = request()->tipo_negocio . '.png';
            $user->password = Hash::make($code);
            $depclientes = DB::table('departamentos')->where('nombre', '=', 'Clientes')->first();
            $user->departamento_id = $depclientes->id;
            $user->save();

            $todayDate = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');

            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Clientes';
            $actividad->registro = request()->nombre . ' ' . request()->apellido_paterno;
            $actividad->accion = 'Creado';
            $actividad->date_time = $todayDate;
            $actividad->save();
            Toastr::success('Cliente Creado', 'Listo');
            return redirect()->route('clientes.index');
        } catch (\Throwable $th) {
            Toastr::error($th->getMessage());
            return redirect()->route('clientes.index');
        }
    }

    // update
    public function updatecliente(Request $request)
    {

        try {
            $user_id = $request->user_id;
            $nombre = $request->nombre;
            $apellido_paterno = $request->apellido_paterno;
            $apellido_materno = $request->apellido_materno;
            $carnet = $request->carnet;
            $telefono = $request->telefono;
            $estado = $request->estado;
            $zona = $request->direccion;
            $tiempo_consumo_turril = $request->tiempo_consumo_turril;
            $tipo_negocio = $request->tipo_negocio;

            $geolocalizacion = $request->geolocalizacion;
            $obs = $request->obs;
            $avatar = $request->tipo_negocio . '.png';

            if (!empty($request->file('foto_negocio'))) {

                $aux = Cliente::find($request->id);
                if ($aux->foto_negocio != null) {
                    unlink('assets/images/clientes/' . $aux->foto_negocio);
                    $request->foto_negocio->move(public_path('assets/images/clientes'), $aux->foto_negocio);
                } else {
                    $foto = time() . '.' . $request->foto_negocio->extension();
                    $request->foto_negocio->move(public_path('assets/images/clientes'), $foto);
                    $aux->foto_negocio = $foto;
                    $aux->save();
                }
            }

            $cli = Cliente::find($request->id);
            $code = "";

            if ($cli->codigo == null || $cli->codigo == '') {
                $userCodeParam = str_pad(request()->user_id, 3, "0", STR_PAD_LEFT);
                $clientCodeParam = str_pad($cli->id, 3, "0", STR_PAD_LEFT);
                $code = $userCodeParam . '-' . $clientCodeParam;

                $cli->codigo = $code;
            } else {
                $code = $cli->codigo;
            }

            $usuario = DB::table('users')->where('email', '=', $code)->first();
            if (!$usuario) {

                $user = new User;
                $user->nombre = $nombre;
                $user->email = $code;
                $user->apellido_paterno = $apellido_paterno;
                $user->apellido_materno = $apellido_materno;
                $user->telefono = $telefono;
                $user->carnet = $carnet;
                $user->direccion = $zona;
                $user->estado = $estado;
                $user->avatar = $avatar;
                // $user->password = $telefono;
                $user['password'] = Hash::make($user['password']);
                $depclientes = DB::table('departamentos')->where('nombre', '=', 'Clientes')->first();
                $user->departamento_id = $depclientes->id;
                $user->save();
            } else {
                $update = [
                    'nombre' => $nombre,
                    'email' => $code,
                    'apellido_paterno' => $apellido_paterno,
                    'apellido_materno' => $apellido_materno,
                    'telefono' => $telefono,
                    'carnet' => $carnet,
                    'direccion' => $zona,
                    'estado' => $estado,
                    'avatar' => $avatar,
                ];
                User::where('id', $usuario->id)->update(['password' => Hash::make($telefono)]);
                User::where('id', $usuario->id)->update($update);
            }
            $update = [
                'user_id'       => $user_id,
                'nombre'         => $nombre,
                'apellido_paterno' => $apellido_paterno,
                'apellido_materno' => $apellido_materno,
                'carnet' => $carnet,
                'telefono' => $telefono,
                'estado' => $estado,
                'zona' => $zona,
                'tiempo_consumo_turril' => $tiempo_consumo_turril,
                'tipo_negocio' => $tipo_negocio,
                'geolocalizacion'   => $geolocalizacion,
                'obs' => $obs,
                'avatar' => $avatar,
                'codigo' => $code,

            ];

            $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
            $todayDate = $dt;
            $actividad = new Registro_cambio();
            $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
            $actividad->codigo = Auth::user()->email;
            $actividad->tabla = 'Clientes';
            $actividad->registro = $nombre . ' ' . $apellido_paterno;
            $actividad->accion = 'Actualizado';
            $actividad->date_time = $todayDate;
            $actividad->save();
            Cliente::where('id', $request->id)->update($update);

            Toastr::success('Cliente Actualizado', 'Listo');
            return redirect()->route('clientes.index');
        } catch (\Throwable $th) {
            Toastr::Error($th->getMessage());
            return redirect()->route('clientes.index');
        }
    }
    // delete
    public function deletecliente(Request $request)
    {
        $clien = Cliente::find($request->id);

        $dt       = Carbon::now()->formatLocalized('%A, %d de %B %Y %H:%M ');
        $todayDate = $dt;
        $actividad = new Registro_cambio();
        $actividad->user_name = Auth::user()->nombre . ' ' . Auth::user()->apellido_paterno;
        $actividad->codigo = Auth::user()->email;
        $actividad->tabla = 'Clientes';
        $actividad->registro = $clien->nombre . ' ' . $clien->apellido_paterno;
        $actividad->accion = 'Eliminado';
        $actividad->date_time = $todayDate;
        $actividad->save();

        Cliente::destroy($request->id);

        Toastr::success('Cliente Eliminado', 'Listo');
        return redirect()->route('clientes.index');
    }
}
