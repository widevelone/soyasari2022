<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportista extends Model
{
    use HasFactory;
    protected $table = 'transportistas';
    
    protected $fillable = [
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'telefono',
    ];

    public function historial()
    {
        return $this->hasMany('App\Models\Historial_almacen', 'transportista_id', 'id');
    }
}
