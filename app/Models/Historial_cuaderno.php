<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_cuaderno extends Model
{
    use HasFactory;
    protected $table = 'historial_cuadernos';
    protected $fillable = [
        "cuaderno_id",
        "cliente_id",
        "producto_id",
        "cantidad",
        "precio_unitario",
        "metodo_pago",
        "metodo_pago_estado",
        "cantidad_vacio",
        "metodo_vacio",
        "estado_turril",
        "total_venta",
        "total_cobrado",
        "total_deuda",  
        "obs",      
        "user_id",
    ];
    public function cuaderno(){
        return $this->hasOne('App\Models\Cuaderno','id','cuaderno_id');
    }
    public function cliente(){
        return $this->hasOne('App\Models\Cliente','id','cliente_id');
    }
    public function producto(){
        return $this->hasOne('App\Models\Producto','id','producto_id');
    }
    public function cuenta(){
        return $this->hasMany('App\Models\Cuenta','historial_cuaderno_id','id');
    }
    public function cuenta_turril(){
        return $this->hasMany('App\Models\Cuenta_turrile','historial_cuaderno_id','id');
    }
    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }



}
