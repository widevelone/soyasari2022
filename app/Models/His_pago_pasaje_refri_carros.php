<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class his_pago_pasaje_refri_carros extends Model
{
    use HasFactory;
    protected $table = 'his_pago_pasaje_refri_carros';

    protected $fillable = [
        'id',
        'pago_pasaje_refri_carros_id',
        'user_id',
        'nro_dias',
        'monto',
        'obs'
    ];

    public function user()
    {
        return $this->hasOne( 'App\Models\User','id',  'user_id');
    }
    
    public function pago_ref_pas_carro()
    {
        return $this->hasOne('App\Models\Pago_ref_pas_carro', 'pago_pasaje_refri_carros_id', 'id');
    }
}
