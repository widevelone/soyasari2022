<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\LockableTrait;

use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    
    use HasFactory, Notifiable, HasRoles;
    use LockableTrait;

    protected $table = "users";
    protected $fillable = [
        'nombre',
        'email',
        'join_date',
        'telefono',
        'estado',
        'role_name',
        'email',
        'role_name',
        'position',
        'departamento_id',
        'password',
    ];
    
    public function departamento(){
        return $this->hasOne('App\Models\Departamento','id','departamento_id');
    }

    public function cliente(){
        return $this->hasMany('App\Models\Cliente','user_id','id'); 
    }

    public function cuaderno(){
        return $this->hasMany('App\Models\Cuaderno','user_id','id'); 
    }




    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
