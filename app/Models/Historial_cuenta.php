<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_cuenta extends Model
{
    use HasFactory;
    protected $fillable = [
        "cuenta_id",
        'cuaderno_id',
        "monto",
        "saldo",
        "obs"
    ];

    public function cuaderno(){
        return $this->hasOne('App\Models\Cuaderno','id','cuaderno_id');
    }

    public function cuenta(){
        return $this->hasOne('App\Models\Cuenta','id','cuenta_id');
    }



}
