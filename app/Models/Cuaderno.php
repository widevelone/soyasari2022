<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuaderno extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'codigo',
        'total_venta',
        'total_cobranza',
        'total_gasto',
        'total_garantia',
        'total_deuda',
        'total_pagos_confirmar',
        'total_cuaderno',
    ];



    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function historial_cuaderno(){
        return $this->hasMany('App\Models\Historial_cuaderno','cuaderno_id','id'); 
    }

    public function historial_cuenta(){
        return $this->hasMany('App\Models\Historial_cuenta','cuaderno_id','id'); 
    }

    public function gastos_cuaderno_diario(){
        return $this->hasMany('App\Models\Gastos_cuaderno_diario','cuaderno_id','id'); 
    }

    public function historial_cuenta_turril(){
        return $this->hasMany('App\Models\Historial_cuenta_turrile','cuaderno_id','id'); 
    }

    
}
