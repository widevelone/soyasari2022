<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pago_ref_pas_carro extends Model
{
    use HasFactory;
    protected $table = 'pago_pasaje_refri_carros';

    protected $fillable = [
        'id',
        'fecha',
        'estado',
        'total',
        'created_at',
        'updated_at'
    ];
}
