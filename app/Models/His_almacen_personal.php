<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class His_almacen_personal extends Model
{
    use HasFactory;
    protected $table = "his_almacenes_personales";
    protected $fillable = [
        'almacen_personal_id',
        'movimiento',
        'user_id',
        'transportista_id',
        'codigo',
        'cantidad',
        'saldo',
        'obs',
    ];
    public function users(){
        return $this->hasOne('App\Models\User','id','user_id');
    }
    public function almacen(){
        return $this->hasOne('App\Models\Almacen','id','almacen_id');
    }
    public function transportista(){
        return $this->hasOne('App\Models\Transportista','id','transportista_id');
    }
}
