<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    use HasFactory;
    protected $fillable = [
        "historial_cuarderno_id",
        "fecha_limite",
        "saldo",
        "estado"
    ];

    public function historial_cuaderno(){
        return $this->hasOne('App\Models\Historial_cuaderno','id','historial_cuaderno_id');
    }
    public function historial_cuenta(){
        return $this->hasMany('App\Models\Historial_cuenta','cuenta_id','id');
    }
    
}
