<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class His_descuento_personal_venta extends Model
{
    use HasFactory;

    protected $table = 'his_descuento_personal_ventas';

    protected $fillable = [
        'id',
        'descuento_personal_venta_id',
        'fecha',
        'detalle',
        'monto',
        'obs'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\Descuento_personal_venta', 'id', 'descuento_personal_venta_id');
    }
}
