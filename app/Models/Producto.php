<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'descripcion',
        'precio',
        'avatar',
        'capacidad',
        'estado'
       ];
 
       public function historial_cuaderno(){
        return $this->hasMany('App\Models\Historial_cuaderno','producto_id','id'); 
    }
}
