<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_cuenta_turrile extends Model
{
    use HasFactory;
    
    
    protected $fillable = [
        'cantidad',
        'cuenta_turril_id',
        'cuaderno_id',      
    ];
    public function cuenta_turril(){
        return $this->hasOne('App\Models\Cuenta_turrile','id','cuenta_turril_id');
    }

    public function cuaderno(){
        return $this->hasOne('App\Models\Cuaderno','id','cuaderno_id');
    }
}
