<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cuenta_turrile extends Model
{
    use HasFactory;
    protected $table = 'cuenta_turriles';
    protected $fillable = [
        'fecha_limite',
        'saldo_cantidad_inicial',
        'saldo_cantidad',
        'saldo_garantia',
        'saldo_garantia_inicial',
        'estado',
        'historial_cuaderno_id',       
    ];
    public function historial_cuaderno(){
        return $this->hasOne('App\Models\Historial_cuaderno','id','historial_cuaderno_id');
    }

    public function historial_cuenta_turril(){
        return $this->hasMany('App\Models\Historial_cuenta_turrile','cuenta_turril_id','id'); 
    }
}
