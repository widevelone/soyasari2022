<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historial_almacen extends Model
{
    use HasFactory;
    protected $fillable = [
        'almacen_id',
        'tipo',
        'user_id',
        'transportista_id',
        'codigo',
        'cantidad',
        'saldo',
        'obs',
    ];
    public function users(){
        return $this->hasOne('App\Models\User','id','user_id');
    }
    public function almacen(){
        return $this->hasOne('App\Models\Almacen','id','almacen_id');
    }
    public function transportista(){
        return $this->hasOne('App\Models\Transportista','id','transportista_id');
    }
    
}
