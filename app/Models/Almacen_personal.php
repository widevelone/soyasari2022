<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Almacen_personal extends Model
{
    use HasFactory;
    protected $table = "almacenes_personales";
    protected $fillable = [
            'producto_id',
            'user_id',
            'estado',
            'cantidad_anterior',
            'cantidad_actual',
    ];

    public function producto(){
        return $this->hasOne('App\Models\Producto','id','producto_id');
    }
    
    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function historial_almacen_personal(){
        return $this->hasMany('App\Models\His_almacen_personal','almacen_personal_id','id'); 
    }
}
