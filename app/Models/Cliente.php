<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory; 
    protected $fillable = [
        'user_id',
        'codigo',
        'nombre',
        'apellido_paterno',
        'apellido_materno',
        'carnet',
        'telefono',
        'estado',
        'zona',
        'tiempo_consumo_turril',
        'tipo_negocio',
        'foto_negocio',
        'geolocalizacion',
        'obs',
    ];
    public function users(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function historial_cuaderno(){
        return $this->hasMany('App\Models\Historial_cuaderno','cliente_id','id'); 
    }
}
