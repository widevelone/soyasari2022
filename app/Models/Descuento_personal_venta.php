<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Descuento_personal_venta extends Model
{
    use HasFactory;
    protected $table = 'descuento_personal_ventas';

    protected $fillable = [
        'id',
        'user_id',
        'estado',
        'total',
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function his_descuento_personal_ventas(){
        return $this->hasMany('App\Models\His_descuento_personal_venta','descuento_personal_venta_id','id'); 
    }
}
